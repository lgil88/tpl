module main

import boolean.Boolean
import boolean.True
import boolean.False
import peano.Zero
import peano.Successor
import peano.Integer

val main = collection.list.reverse[peano.Integer](collection.list.range(peano.constants.Ten))
val main2 = collection.list.range(peano.constants.Ten).size()

val access = collection.list.Cons[peano.Integer](peano.constants.Three, collection.list.Nil).head

val tupl = (peano.Zero, peano.Zero)

val output = (x: Integer) => Zero