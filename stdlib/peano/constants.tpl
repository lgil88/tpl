module peano.constants

import peano.Successor
import peano.Zero

val One = Successor(Zero)
val Two = Successor(One)
val Three = Successor(Two)
val Four = Successor(Three)
val Five = Successor(Four)
val Six = Successor(Five)
val Seven = Successor(Six)
val Eight = Successor(Seven)
val Nine = Successor(Eight)
val Ten = Successor(Nine)