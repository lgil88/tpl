module boolean

trait Boolean
object True extends Boolean
object False extends Boolean

def Not(b: Boolean): Boolean =
    b match {
        case (True) => False
        case (False) => True
    }
    
def equals(a: Any, b: Any): Boolean = a match {
  case (b) => True
  case _ => False
}

def choose[T](c: Boolean, then_option: () => T, else_option: () => T): T = c match {
    case (True) => then_option()
    case (False) => else_option()
}