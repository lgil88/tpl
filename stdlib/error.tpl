module error

def error(): Nothing = () match {} // This forces a match error to happen. A call to this function can be used as a placeholder for stuff that is not yet implemented
def loop(): Nothing = loop()
def todo(): Nothing = () match {}