module peano

trait Integer {
    def next(): Integer = Successor(this)
}
object Zero extends Integer
class Successor(predecessor: Integer) extends Integer

def Add(lhs: Integer, rhs: Integer): Integer = (lhs, rhs) match {
    case tuple(lhs, (Zero)) => lhs
    case tuple(lhs, Successor(predecessor)) => Add(Successor(lhs), predecessor)
}

def Sub(lhs: Integer, rhs: Integer): Integer = (lhs, rhs) match {
    case tuple(lhs, (Zero)) => lhs
    case tuple(Successor(pre1), Successor(pre2)) => Sub(pre1, pre2)
}

def Mul(lhs: Integer, rhs: Integer): Integer = (lhs, rhs) match {
    case tuple(_, (Zero)) => Zero
    case tuple(lhs, (constants.One)) => lhs
    case tuple(lhs, Successor(predecessor)) => Add(lhs, Mul(lhs, predecessor))
}

def greaterEquals(lhs: Integer, rhs: Integer): boolean.Boolean = (lhs, rhs) match {
    case tuple(_, (Zero)) => boolean.True
    case tuple((Zero), _) => boolean.False
    case tuple(Successor(l), Successor(r)) => greaterEquals(l, r)
}

def Div(lhs: Integer, rhs: Integer): Integer = boolean.choose[Integer](greaterEquals(lhs, rhs),
    () => Successor(Div(Sub(lhs, rhs), rhs)),
    () => Zero
)

def isOdd(x: Integer): boolean.Boolean = x match {
    case (Zero) => boolean.False
    case Successor(pre) => isEven(pre)
}

def isEven(x: Integer): boolean.Boolean = x match {
    case (Zero) => boolean.True
    case Successor(pre) => isOdd(pre)
}