module collection.list

import collection.option.Option
import collection.option.None
import collection.option.Some
import peano.Integer
import peano.Successor
import peano.Zero

trait List[+T]{
    def fold_left[A](start: A, f: (A, T) => A): A
    def size(): peano.Integer
    def map[U](f: (T) => U): List[U]

    def last(): T
    def init(): List[T]
    def get(i: peano.Integer): T
    def flatMap[U](f: (T) => List[U]): List[U]
    def filter(f: (T) => boolean.Boolean): List[T]

    def take(n: peano.Integer): List[T]
    def takeWhile(f: (T) => boolean.Boolean): List[T]
    def dropLeft(n: peano.Integer): List[T]

    //def headOption() : Option[T]
}

object Nil extends List[Nothing] {
    def fold_left[A](start: A, f: (A, Nothing) => A): A = start
    def size(): peano.Integer = peano.Zero
    def map[U](f: (Nothing) => U): List[U] = Nil

    def last(): Nothing = error.error()
    def init(): List[Nothing] = error.error()
    def get(i: peano.Integer): Nothing = error.error()
    def flatMap[U](f: (Nothing) => List[U]): List[U] = Nil
    def filter(f: (Nothing) => boolean.Boolean): List[Nothing] = Nil

    def take(n: peano.Integer): List[Nothing] = Nil
    def takeWhile(f: (Nothing) => boolean.Boolean): List[Nothing] = Nil
    def dropLeft(n: peano.Integer): List[Nothing] = Nil


    //def headOption() : Option[Nothing] = None
}
class Cons[+T](head: T, tail: List[T]) extends List[T] {
    def fold_left[A](start: A, f: (A, T) => A): A = this.tail.fold_left[A](f(start, this.head), f)
    def size(): peano.Integer = peano.Successor(this.tail.size())
    def map[U](f: (T) => U): List[U] = Cons[U](f(this.head), this.tail.map[U](f))

    def last(): T = this.tail match {
        case (Nil) => this.head
        case _ => this.tail.last()
    }
    def init(): List[T] = error.todo()

    def get(i: peano.Integer): T = i match {
        case (peano.Zero) => this.head
        case Successor(pre) => this.tail.get(pre)
    }
    def flatMap[U](f: (T) => List[U]): List[U] = flatten[U](this.map[List[U]](f))
    def filter(f: (T) => boolean.Boolean): List[T] = f(this.head) match {
        case (boolean.True) => Cons[T](this.head, this.tail.filter(f))
        case (boolean.False) => this.tail.filter(f)
    }

    def take(n: peano.Integer): List[T] = n match {
        case (Zero) => Nil
        case Successor(pre) => Cons[T](this.head, this.tail.take(pre))
    }
    def takeWhile(f: (T) => boolean.Boolean): List[T] = f(this.head) match {
        case (boolean.True) => Cons[T](this.head, this.tail.takeWhile(f))
        case (boolean.False) => Nil
    }
    def dropLeft(n: peano.Integer): List[T] = n match {
        case (peano.Zero) => this
        case peano.Successor(pre) => this.tail.dropLeft(pre)
    }

    //def headOption() : Option[T] = Some[T](this.head)
}

def concat[B](a: List[B], b: List[B]): List[B] = reverse[B](a).fold_left[List[B]](b, swap[B, List[B], List[B]](Cons[B]))

def flatten[T](list: List[List[T]]): List[T] = list.fold_left[List[T]](Nil, concat[T])



def fold_left[A, B](start: A, list: List[B], f: (A, B) => A): A = list.fold_left[A](start, f)

def swap[A, B, C](f: (A, B) => C): (B, A) => C = (b: B, a: A) => f(a, b)

def reverse[A](list: List[A]): List[A] = list.fold_left[List[A]](Nil, swap[A, List[A], List[A]](Cons[A]))

def range2(start: peano.Integer, inclusive_end: peano.Integer): List[peano.Integer] = {
    def helper(n: peano.Integer): List[peano.Integer] =
        boolean.choose[List[peano.Integer]](boolean.equals(n, inclusive_end),
            () => Cons[peano.Integer](n, Nil),
            () => Cons[peano.Integer](n, helper(Successor(n))))

    helper(start)
}

def range(inclusive_end: peano.Integer): List[peano.Integer] = range2(peano.Zero, inclusive_end)

def fill[T](count: peano.Integer): (T) => List[T] = (e: T) => count match {
    case (peano.Zero) => Nil
    case peano.Successor(pre) => Cons[T](e, fill[T](pre)(e))
}