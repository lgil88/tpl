module collection.option

import boolean.True
import boolean.False
import boolean.Boolean

trait Option[+A]{
    def isDefined(): Boolean
    def map[B](f: (A) => B): Option[B]
    def flatMap[B](f: (A) => Option[B]): Option[B]
}
object None extends Option[Nothing] {
    def isDefined(): Boolean = boolean.False
    def map[B](f: (Nothing) => B): Option[B] = this
    def flatMap[B](f: (Nothing) => Option[B]): Option[B] = this
}
class Some[+A](value: A) extends Option[A] {
    def isDefined(): boolean.Boolean = boolean.True
    def map[B](f: (A) => B): Option[B] = Some[B](f(this.value))
    def flatMap[B](f: (A) => Option[B]): Option[B] = f(this.value)
}