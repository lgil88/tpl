module peano2

trait Integer
object Zero extends Integer
class Successor(predecessor: Integer) extends Integer

val One = Successor(Zero)

def identity_tuple[A](value: A): tuple[A, A] = (value, value) 

def Add(lhs: Integer, rhs: Integer): Integer = (lhs, rhs) match {
    case tuple(lhs, (Zero)) => lhs
    case tuple(lhs, Successor(predecessor)) => Add(Successor(lhs), predecessor)
}

val test = Add(Zero, One)

val lambda = (a: Integer) => a

def nullary(): Integer = Zero

val nullary_call = nullary()

trait List[+A]{
    def size(): Integer
}
class Cons[+A](head: A, tail: List[A]) extends List[A]{
    def size(): Integer = Successor(this.tail.size())
}
object Nil extends List[Nothing] {
    def size(): Integer = Zero
}

class Test(f: (Integer) => Integer)

val t = (x: Integer) => (x: Integer) => x

val full = Zero match {
    case (Zero) => Zero
}

val c = (x: Integer) => {
    val x = Zero
    
    x
}