name := "TPL"

version := "0.1"

scalaVersion := "2.13.5"

libraryDependencies += "org.scala-lang.modules" %% "scala-parser-combinators" % "1.1.2"

libraryDependencies += "org.scalactic" %% "scalactic" % "3.2.12"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.12" % "test"
libraryDependencies += "org.typelevel" %% "cats-effect" % "3.3.14"
libraryDependencies += "org.apache.commons" % "commons-text" % "1.9"

Compile / resourceDirectory := baseDirectory.value / "src" / "main" / "ressources"
Test / resourceDirectory := baseDirectory.value / "src" / "test" / "ressources"

assemblyJarName := "tpl.jar"

logBuffered in Test := false