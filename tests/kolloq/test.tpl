// name: Ackermann
// stdlib: true
// outcome: success
// output: Successor(Successor(Successor(Successor(Zero))))

module test

import peano.Integer
import peano.Zero
import peano.Successor

def ackermann(a: Integer, b: Integer): Integer = (a, b) match {
    case tuple((Zero), n) => Successor(n)
    case tuple(Successor(m), (Zero)) => ackermann(m, peano.constants.One)
    case tuple(Successor(m), Successor(n)) => ackermann(m, ackermann(Successor(m), n))
}

val main = ackermann(peano.constants.One, peano.constants.Two)