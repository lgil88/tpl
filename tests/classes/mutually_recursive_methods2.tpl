// outcome: success
// output: True
// stdlib: true

module test

import peano.Integer
import peano.Zero
import peano.Successor

import boolean.Boolean
import boolean.True
import boolean.False

class Foo(n: Integer) {
        def isOdd(): Boolean = this.n match {
           case (Zero) => False
           case Successor(pre) => Foo(pre).isEven()
        }

        def isEven(): Boolean = this.n match {
           case (Zero) => True
           case Successor(pre) => Foo(pre).isOdd()
        }
}

val main = Foo(peano.constants.Ten).isEven()

