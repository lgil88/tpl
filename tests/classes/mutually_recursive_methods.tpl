// outcome: success
// output: True
// stdlib: true

module test

import peano.Integer
import peano.Zero
import peano.Successor

import boolean.Boolean
import boolean.True
import boolean.False

class Foo() {
        def isOdd(n: Integer): Boolean = n match {
           case (Zero) => False
           case Successor(pre) => this.isEven(pre)
        }

        def isEven(n: Integer): Boolean = n match {
           case (Zero) => True
           case Successor(pre) => this.isOdd(pre)
        }
}

val main = Foo().isEven(peano.constants.Ten)

