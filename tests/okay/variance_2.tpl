// stdlib: false
// outcome: compilersuccess

module test

trait Animal
class Dog() extends Animal

trait List[+T]
object Nil extends List[Nothing]
class Cons[+T](head: T, tail: List[T]) extends List[T]

def foo(): List[Animal] = Cons[Dog](Dog(), Nil)
