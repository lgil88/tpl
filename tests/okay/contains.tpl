// stdlib: false
// outcome: compilersuccess

module test

trait Boolean

trait List[+T]{
    def contains(v: Any): Boolean
}