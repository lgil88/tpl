// stdlib: false
// outcome: compilersuccess

module list

trait Integer
object Zero extends Integer
class Successor(predecessor: Integer) extends Integer

trait List[+T]{
    def fold_left[A](start: A, f: (A, T) => A): A
    def size(): Integer
}

object Nil extends List[Nothing] {
    def fold_left[B](start: B, f: (B, Nothing) => B): B = start
    def size(): Integer = Zero
}

class Cons[+T](head: T, tail: List[T]) extends List[T] {
    def fold_left[A](start: A, f: (A, T) => A): A = this.tail.fold_left[A](f(start, this.head), f)
    def size(): Integer = Successor(this.tail.size())
}