// stdlib: false
// outcome: compilersuccess

module test

def identity_tuple[A](value: A): tuple[A, A] = (value, value) 