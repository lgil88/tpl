// stdlib: false
// outcome: compilersuccess

module generics

trait A[T]
trait C[T] extends A[(T) => T]