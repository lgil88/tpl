// stdlib: false
// outcome: compilererror
// output: Class Derived does not override abstract method 'method' [(Int) => Int]

module test

trait Int
trait Base{
    def method(a: Int): Int
}
class Derived() extends Base