// stdlib: false
// outcome: compilererror
// output: error: Class Nil does not override abstract method 'contains' [[U] -> (U) => Bool]

module test 

trait Bool
object True extends Bool
object False extends Bool

trait List[+T]{
    def contains[U](v: U): Bool
}
object Nil extends List[Nothing]
class Cons[+T](head: T, tail: List[T]) extends List[T]{
    def contains[U](v: U): Bool = False
}