// stdlib: false
// outcome: compilererror
// output: Method type (Dog) => Animal does not override (Animal) => Dog for method method

module test

trait Animal
class Dog() extends Animal

trait Base {
    def method(a: Animal): Dog
}
class Derived() extends Base{
    def method(a: Dog) : Animal = Dog()
}