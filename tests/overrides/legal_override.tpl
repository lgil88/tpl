// stdlib: true
// outcome: compilersuccess

module test

trait Animal
class Dog() extends Animal

trait Base {
    def method(a: Dog): Animal
}
class Derived() extends Base{
    def method(a: Animal) : Dog = Dog()
}