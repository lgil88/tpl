// name: Invariance 1 
// stdlib: false
// outcome: compilererror
// output: error: Type mismatch. Expected: Invariant[Base], got Any

module test

class Invariant[T]()

trait Base
trait Derived extends Base

val result: Invariant[Base] = () match {
    case tuple() => Invariant[Base]()
    case tuple() => Invariant[Derived]() 
}