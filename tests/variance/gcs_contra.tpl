// name: Greatest Common Subtype contravariance
// stdlib: false
// outcome: compilererror
// output: error: Type mismatch. Expected: Nothing, got Contra[List[Derived]]

module test

class Contra[-T]()

trait Seq[+T]
trait List[+T] extends Seq[T]

trait Base
trait Derived extends Base

val result: Nothing = () match {
    case tuple() => Contra[Seq[Base]]()
    case tuple() => Contra[List[Derived]]()
}