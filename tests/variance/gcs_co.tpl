// name: Greatest Common Subtype contravariance
// stdlib: false
// outcome: compilererror
// output: error: Type mismatch. Expected: Nothing, got Co[Seq[Base]]

module test

class Contra[-T]()
class Co[+T]()

trait Seq[+T]
trait List[+T] extends Seq[T]

trait Base
trait Derived extends Base

val result: Nothing = () match {
    case tuple() => Co[Seq[Derived]]()
    case tuple() => Co[List[Base]]()
}