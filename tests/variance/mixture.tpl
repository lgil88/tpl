// stdlib: true
// outcome: compilersuccess

module test

trait Foo[-T, +U, +I1, -I2] {
    def f(p: T) : (T) => U
}

class Bar[-T, +U, I](v: U, v2: (T) => U, i: I) extends Foo[T, U, I, I] {
    def f(p: T) : (T) => U = error.error() 
}