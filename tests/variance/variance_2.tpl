// name: Covariant at Contravariant position 2
// stdlib: false
// outcome: compilererror
// output: Covariant T at contravariant position

module test

class Class[+T]() {
    def foo(t: T): Class[T] = Class[T]()
}

