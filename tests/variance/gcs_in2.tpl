// name: Greatest Common Subtype invariance 2
// stdlib: true
// outcome: compilererror
// output: error: Type mismatch. Expected: Nothing, got B[Cat]

module test

trait Animal
trait Dog extends Animal
trait Cat extends Animal

trait A[T]
trait B[T] extends A[T]
trait C[T] extends A[T]

val a: B[Cat] = error.error()
val b: B[Cat] = error.error()

val result: Nothing = () match {
    case tuple() => a
    case tuple() => b
}