// name: Covariant at Contravariant position 1
// stdlib: false
// outcome: compilererror
// output: Covariant T at contravariant position

module test

trait List[-T]
object Nil extends List[Nothing]
class Cons[+T](head: T, tail: List[T]) extends List[T]

