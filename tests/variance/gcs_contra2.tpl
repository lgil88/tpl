// name: Greatest Common Subtype contravariance 2
// stdlib: false
// outcome: compilererror
// output: error: Type mismatch. Expected: Nothing, got (List[Derived]) => Zero

module test

class Contra[-T]()

trait Seq[+T]
trait List[+T] extends Seq[T]

trait Base
trait Derived extends Base

object Zero

val result: Nothing = () match {
    case tuple() => (a: Seq[Base]) => Zero
    case tuple() => (a: List[Derived]) => Zero
}