// name: Tuple Match
// stdlib: false
// outcome: success
// output = ()

module test

val a: Any = ()

val main = a match {
    case tuple() => a
}