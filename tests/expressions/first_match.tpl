// name: First Match instead of most specific match
// stdlib: true
// outcome: success
// output: True

module test

import peano.Integer
import peano.Zero
import peano.Successor

import boolean.Boolean
import boolean.True
import boolean.False

val main: Boolean =
    peano.constants.Ten match {
        case Successor(_) => True
        case Successor(Successor(_)) => False
    }