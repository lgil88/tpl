// name: Empty Match
// stdlib: true
// outcome: success
// output: False

module test

val matchee = collection.list.Cons[collection.list.List[peano.Integer]](collection.list.Cons[peano.Integer](peano.constants.One, collection.list.Nil), collection.list.Cons[collection.list.List[peano.Integer]](collection.list.Nil, collection.list.Nil))

val main = matchee match {
    case collection.list.Cons[collection.list.List[peano.Integer]](head, tail) => boolean.False
    case collection.list.Cons[collection.list.List[peano.Integer]](collection.list.Cons[peano.Integer](head, tail), tail2) => boolean.False
    case collection.list.Cons[collection.list.List[peano.Integer]](head, collection.list.Cons[collection.list.List[peano.Integer]](head2, tail)) => boolean.True
}