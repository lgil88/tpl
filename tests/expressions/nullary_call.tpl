// name: StdLib Template
// stdlib: true
// outcome: success
// output: Zero

module test

def nullary(): peano.Integer = peano.Zero

val main = nullary()