// stdlib: true
// outcome: compilersuccess

module test

import error.error
import peano.Integer
import peano.Zero
import collection.list.List
import collection.list.fold_left

val list: List[List[peano.Integer]] = error()

val main = collection.list.fold_left[Integer, Integer](Zero, list.map[Integer]((v: List[Integer]) => v.size()), peano.Add)