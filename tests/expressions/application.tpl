// name:
// stdlib: true
// outcome: success
// output: Zero

module test

import peano.Integer
import peano.Zero
import peano.Successor

import boolean.Boolean
import boolean.True
import boolean.False

val main: peano.Integer = ((a: (peano.Integer) => peano.Integer) => a)((a: peano.Integer) => a)(peano.Zero)