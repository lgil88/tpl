// name: Local Mutual Recursion
// stdlib: true
// outcome: success
// output: (True, False)

module test

val main = {
    import peano.Integer
    import peano.Zero
    import peano.Successor
    
    import boolean.Boolean
    import boolean.True
    import boolean.False
    
    def isOdd(n: Integer): Boolean = n match {
       case (Zero) => False
       case Successor(pre) => isEven(pre)
    }
    
    def isEven(n: Integer): Boolean = n match {
       case (Zero) => True
       case Successor(pre) => isOdd(pre)
    }
    
    (isOdd(peano.constants.Nine), isEven(peano.constants.Nine))
}