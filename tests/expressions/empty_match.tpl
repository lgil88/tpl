// name: Empty Match
// stdlib: false
// outcome: runtimeerror

module test

object Val

def test(): Nothing = Val match {}
