// name: Empty Match
// stdlib: true
// outcome: success
// output: <function main>

module test

def main(): peano.Integer = peano.Zero