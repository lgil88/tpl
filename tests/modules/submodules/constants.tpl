module peano.constants

import peano.Successor
import peano.Zero

val One = Successor(Zero)
val Two = Successor(One)