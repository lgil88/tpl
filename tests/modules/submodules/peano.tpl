module peano

trait Integer
class Successor(pre: Integer) extends Integer
object Zero extends Integer