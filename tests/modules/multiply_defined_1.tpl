// stdlib: false
// outcome: compilererror
// output:  is already defined at

module test

object Foo

val foo = Foo
val foo = Foo
