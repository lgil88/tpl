// name: Illegal Inheritance 2
// stdlib: false
// outcome: compilererror
// output: Type (T) => T is not a class type.

module generics

trait C[T] extends (T) => T