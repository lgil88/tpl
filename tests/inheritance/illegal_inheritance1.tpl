// name: Illegal Inheritance 1
// stdlib: false
// outcome: compilererror
// output: Type T is not a class type.

module generics

trait C[T] extends T