// name: Haskell 7
// stdlib: true
// outcome: success
// output: Cons(Successor(Zero), Cons(Successor(Zero), Cons(Successor(Zero), Cons(Successor(Zero), Cons(Successor(Successor(Zero)), Cons(Successor(Successor(Successor(Zero))), Cons(Successor(Successor(Successor(Zero))), Cons(Successor(Zero), Cons(Successor(Zero), Cons(Successor(Successor(Successor(Successor(Zero)))), Cons(Successor(Successor(Successor(Successor(Successor(Zero))))), Cons(Successor(Successor(Successor(Successor(Successor(Zero))))), Cons(Successor(Successor(Successor(Successor(Successor(Zero))))), Cons(Successor(Successor(Successor(Successor(Successor(Zero))))), Nil))))))))))))))

module test

import collection.list.List
import collection.list.Nil
import collection.list.Cons
import collection.list.reverse
import peano.constants
import peano.Integer
import peano.Zero
import peano.Successor
import boolean.equals

// [1, 2, 2, 3, 4]
val data =
Cons[Integer](constants.One,
Cons[Integer](constants.One,
Cons[Integer](constants.One,
Cons[Integer](constants.One,
Cons[Integer](constants.Two,
Cons[Integer](constants.Three,
Cons[Integer](constants.Three,
Cons[Integer](constants.One,
Cons[Integer](constants.One,
Cons[Integer](constants.Four,
Cons[Integer](constants.Five,
Cons[Integer](constants.Five,
Cons[Integer](constants.Five,
Cons[Integer](constants.Five,
Nil))))))))))))))

trait RleElement[+T]{
    def decode(): List[T]
}
class SimpleRle[T](e: T) extends RleElement[T] {
    def decode(): List[T] = collection.list.fill[T](constants.One)(this.e)
}
class RleRle[T](c: Integer, e: T) extends RleElement[T]{
    def decode(): List[T] = collection.list.fill[T](this.c)(this.e)
}

def pack[T](list: List[T]) : List[List[T]] = list match {
        case (Nil) => Nil
        case Cons[T](head, tail) => {
            val taken = list.takeWhile((e: T) => equals(head, e))

            Cons[List[T]](taken, pack[T](list.dropLeft(taken.size())))
        }
    }

def rle[T](list: List[T]): List[tuple[Integer, T]] = {
    pack[T](list).map[tuple[Integer, T]]((e: List[T]) => (e.size(), e.get(Zero)))
}

def rle2[T](list: List[T]): List[RleElement[T]] = rle[T](list).map[RleElement[T]]((e: tuple[Integer, T]) => e match {
    case tuple((constants.One), e) => SimpleRle[T](e)
    case tuple(c, e) => RleRle[T](c, e)
})

def decode[T](list: List[RleElement[T]]): List[T] =
    collection.list.flatten[T](list.map[List[T]]((e: RleElement[T]) => e.decode()))

val main = decode[Integer](rle2[Integer](data))
