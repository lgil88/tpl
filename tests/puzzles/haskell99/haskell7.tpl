// name: Haskell 7
// stdlib: true
// outcome: success
// output: True

module test

import collection.list.List
import collection.list.Nil
import collection.list.Cons
import collection.list.reverse
import peano.constants
import peano.Integer
import boolean.equals

val a = collection.list.range2(peano.Zero, constants.Two)
val b = collection.list.range2(constants.Three, constants.Five)
val c = collection.list.range2(constants.Six, constants.Eight)
val d = collection.list.range2(constants.Nine, constants.Ten)



trait Tree
class Element(e: Integer) extends Tree
class Inner(children: List[Tree]) extends Tree

// I'm sorry. (1, (2, (3, (4, 5))))
val data =
Inner(Cons[Tree](Element(constants.One), Cons[Tree](
Inner(Cons[Tree](Element(constants.Two), Cons[Tree](
Inner(Cons[Tree](Element(constants.Three), Cons[Tree](
Inner(Cons[Tree](Element(constants.Four), Cons[Tree](
    Element(constants.Five)
, Nil)))
, Nil)))
, Nil)))
, Nil)))

def flatten(t: Tree): List[Integer] = t match {
    case Element(e) => Cons[Integer](e, Nil)
    case Inner(children) => collection.list.flatten[Integer](children.map[List[Integer]](flatten))
}

val main = equals(flatten(data), collection.list.range2(constants.One, constants.Five))
