// name: Haskell 7
// stdlib: true
// outcome: success
//output: Cons(Zero, Cons(Successor(Successor(Zero)), Cons(Successor(Successor(Successor(Successor(Zero)))), Nil)))


module test

import collection.list.List
import collection.list.Nil
import collection.list.Cons
import collection.list.reverse
import peano.constants
import peano.Integer
import peano.Zero
import peano.Successor
import boolean.equals

val data = Cons[Integer](Zero, Cons[Integer](constants.One, Cons[Integer](constants.Two, Cons[Integer](constants.Three, Cons[Integer](constants.Four, Nil)))))

def dropEveryNth[T](list: List[T], n: peano.Integer): List[T] = boolean.choose[List[T]](peano.greaterEquals(list.size(), n),
    () => collection.list.concat[T](list.take(peano.Sub(n, constants.One)), dropEveryNth[T](list.dropLeft(n), n)),
    () => list
)

val main = dropEveryNth[Integer](data, constants.Two)
