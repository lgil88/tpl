// name: Haskell 5
// stdlib: true
// outcome: success
// output: Cons(Successor(Successor(Successor(Successor(Zero)))), Cons(Successor(Successor(Successor(Zero))), Cons(Successor(Successor(Zero)), Cons(Successor(Zero), Nil))))

module test

import collection.list.List
import collection.list.Nil
import collection.list.Cons
import peano.constants
import peano.Integer

// [1, 2, 3, 4]
val data = Cons[Integer](constants.One, Cons[Integer](constants.Two, Cons[Integer](constants.Three, Cons[Integer](constants.Four, Nil))))

val main = collection.list.reverse[Integer](data)