// name: Haskell 5
// stdlib: true
// outcome: success
// output: (False, True, True)

module test

import collection.list.List
import collection.list.Nil
import collection.list.Cons
import collection.list.reverse
import peano.constants
import peano.Integer
import boolean.equals

// [1, 2, 3, 4]
val data = Cons[Integer](constants.One, Cons[Integer](constants.Two, Cons[Integer](constants.Three, Cons[Integer](constants.Four, Nil))))
val palindrome = Cons[Integer](constants.One, Cons[Integer](constants.Two, Cons[Integer](constants.Two, Cons[Integer](constants.One, Nil))))
val palindrome2 = Cons[Integer](constants.One, Cons[Integer](constants.Two, Cons[Integer](constants.Three, Cons[Integer](constants.Two, Cons[Integer](constants.One, Nil)))))

def isPalindrome[T](l: List[T]): boolean.Boolean = equals(l, reverse[T](l))

val main = (
                isPalindrome[Integer](data),
                isPalindrome[Integer](palindrome),
                isPalindrome[Integer](palindrome2)
           )

