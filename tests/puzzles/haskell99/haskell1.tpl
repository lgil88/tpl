// name: Haskell 1
// stdlib: true
// outcome: success
// output: Successor(Successor(Successor(Successor(Zero))))

module test

import collection.list.List
import collection.list.Nil
import collection.list.Cons
import peano.constants
import peano.Integer

// [1, 2, 3, 4]
val data = Cons[Integer](constants.One, Cons[Integer](constants.Two, Cons[Integer](constants.Three, Cons[Integer](constants.Four, Nil))))

val main = data.last()