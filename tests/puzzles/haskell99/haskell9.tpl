// name: Haskell 7
// stdlib: true
// outcome: success
// output: Cons(Cons(Successor(Zero), Cons(Successor(Zero), Cons(Successor(Zero), Cons(Successor(Zero), Nil)))), Cons(Cons(Successor(Successor(Zero)), Nil), Cons(Cons(Successor(Successor(Successor(Zero))), Cons(Successor(Successor(Successor(Zero))), Nil)), Cons(Cons(Successor(Zero), Cons(Successor(Zero), Nil)), Cons(Cons(Successor(Successor(Successor(Successor(Zero)))), Nil), Cons(Cons(Successor(Successor(Successor(Successor(Successor(Zero))))), Cons(Successor(Successor(Successor(Successor(Successor(Zero))))), Cons(Successor(Successor(Successor(Successor(Successor(Zero))))), Cons(Successor(Successor(Successor(Successor(Successor(Zero))))), Nil)))), Nil))))))


module test

import collection.list.List
import collection.list.Nil
import collection.list.Cons
import collection.list.reverse
import peano.constants
import peano.Integer
import boolean.equals

// [1, 2, 2, 3, 4]
val data =
Cons[Integer](constants.One,
Cons[Integer](constants.One,
Cons[Integer](constants.One,
Cons[Integer](constants.One,
Cons[Integer](constants.Two,
Cons[Integer](constants.Three,
Cons[Integer](constants.Three,
Cons[Integer](constants.One,
Cons[Integer](constants.One,
Cons[Integer](constants.Four,
Cons[Integer](constants.Five,
Cons[Integer](constants.Five,
Cons[Integer](constants.Five,
Cons[Integer](constants.Five,
Nil))))))))))))))

def pack[T](list: List[T]) : List[List[T]] = list match {
        case (Nil) => Nil
        case Cons[T](head, tail) => {
            val taken = list.takeWhile((e: T) => equals(head, e))

            Cons[List[T]](taken, pack[T](list.dropLeft(taken.size())))
        }
    }


val main = pack[Integer](data)
