// name: Haskell 7
// stdlib: true
// outcome: success
//output: Cons(Successor(Zero), Cons(Successor(Zero), Cons(Successor(Zero), Cons(Successor(Successor(Zero)), Cons(Successor(Successor(Zero)), Cons(Successor(Successor(Zero)), Cons(Successor(Successor(Successor(Zero))), Cons(Successor(Successor(Successor(Zero))), Cons(Successor(Successor(Successor(Zero))), Cons(Successor(Successor(Successor(Successor(Zero)))), Cons(Successor(Successor(Successor(Successor(Zero)))), Cons(Successor(Successor(Successor(Successor(Zero)))), Nil))))))))))))


module test

import collection.list.List
import collection.list.Nil
import collection.list.Cons
import collection.list.reverse
import peano.constants
import peano.Integer
import peano.Zero
import peano.Successor
import boolean.equals

val data = Cons[Integer](constants.One, Cons[Integer](constants.Two, Cons[Integer](constants.Three, Cons[Integer](constants.Four, Nil))))

def replicate[T](c: peano.Integer): (List[T]) => List[T] = (l: List[T]) => collection.list.flatten[T](l.map[List[T]](collection.list.fill[T](c)))

val main = replicate[Integer](constants.Three)(data)
