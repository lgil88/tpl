// name: Haskell 7
// stdlib: true
// outcome: success
// output: Cons(Successor(Zero), Cons(Successor(Successor(Zero)), Cons(Successor(Successor(Successor(Zero))), Cons(Successor(Successor(Successor(Successor(Zero)))), Nil))))

module test

import collection.list.List
import collection.list.Nil
import collection.list.Cons
import collection.list.reverse
import peano.constants
import peano.Integer
import boolean.equals

// [1, 2, 2, 3, 4]
val data = Cons[Integer](constants.One, Cons[Integer](constants.Two, Cons[Integer](constants.Two, Cons[Integer](constants.Three, Cons[Integer](constants.Four, Nil)))))

def eliminate[T](list: List[T]): List[T] = {
    def helper(current: T, rem: List[T]): List[T] = rem match {
        case (Nil) => Nil
        case Cons[T](head, tail) =>
            boolean.choose[List[T]](boolean.equals(current, head),
                () => helper(current, tail),
                () => Cons[T](head, helper(head, tail))
            )
    }

    list match {
        case (Nil) => Nil
        case Cons[T](head, tail) => Cons[T](head, helper(head, tail))
    }
}

val main = eliminate[Integer](data)

