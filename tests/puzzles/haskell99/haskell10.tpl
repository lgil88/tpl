// name: Haskell 7
// stdlib: true
// outcome: success
// output: Cons((Successor(Successor(Successor(Successor(Zero)))), Successor(Zero)), Cons((Successor(Zero), Successor(Successor(Zero))), Cons((Successor(Successor(Zero)), Successor(Successor(Successor(Zero)))), Cons((Successor(Successor(Zero)), Successor(Zero)), Cons((Successor(Zero), Successor(Successor(Successor(Successor(Zero))))), Cons((Successor(Successor(Successor(Successor(Zero)))), Successor(Successor(Successor(Successor(Successor(Zero)))))), Nil))))))

module test

import collection.list.List
import collection.list.Nil
import collection.list.Cons
import collection.list.reverse
import peano.constants
import peano.Integer
import peano.Zero
import peano.Successor
import boolean.equals

// [1, 2, 2, 3, 4]
val data =
Cons[Integer](constants.One,
Cons[Integer](constants.One,
Cons[Integer](constants.One,
Cons[Integer](constants.One,
Cons[Integer](constants.Two,
Cons[Integer](constants.Three,
Cons[Integer](constants.Three,
Cons[Integer](constants.One,
Cons[Integer](constants.One,
Cons[Integer](constants.Four,
Cons[Integer](constants.Five,
Cons[Integer](constants.Five,
Cons[Integer](constants.Five,
Cons[Integer](constants.Five,
Nil))))))))))))))

def pack[T](list: List[T]) : List[List[T]] = list match {
        case (Nil) => Nil
        case Cons[T](head, tail) => {
            val taken = list.takeWhile((e: T) => equals(head, e))

            Cons[List[T]](taken, pack[T](list.dropLeft(taken.size())))
        }
    }

def rle[T](list: List[T]): List[tuple[Integer, T]] = {
    pack[T](list).map[tuple[Integer, T]]((e: List[T]) => (e.size(), e.get(Zero)))
}

val main = rle[Integer](data)
