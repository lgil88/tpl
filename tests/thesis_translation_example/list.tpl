module list

trait List[+T]{
    def size(): peano.Integer
}

object Nil extends List[Nothing] {
    def size(): peano.Integer = peano.Zero
}

class Cons[+T](head: T, tail: List[T]) extends List[T]{
    def size(): peano.Integer = peano.Successor(this.tail.size())
}