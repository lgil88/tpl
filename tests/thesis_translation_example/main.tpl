module main2

val main = {    
    import peano.Integer
    import peano.Zero
    import peano.Successor

    def isOdd(n: Integer): boolean.Boolean = n match {
        case (Zero) => boolean.False
        case Successor(pre) => isEven(pre)
    }
    
    def isEven(n: Integer): boolean.Boolean = n match {
        case (Zero) => boolean.True
        case Successor(pre) => isOdd(pre)
    }

    isEven(peano.One)
}

val binary_tuple = (boolean.True, boolean.False)
