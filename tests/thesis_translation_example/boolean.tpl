module boolean

trait Boolean
object True extends Boolean
object False extends Boolean