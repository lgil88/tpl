module peano
    
trait Integer
object Zero extends Integer
class Successor(predecessor: Integer) extends Integer

val One = Successor(Zero)