
<!-- PROJECT LOGO -->
<br />
<div align="center">
<h3 align="center">TPL</h3>

  <p align="center">
    A compiler to generate template metaprograms in C++.
    <br />
    <br />
    <a href="https://git.thm.de/lgil88/tpl/-/issues">Report Bug</a>
    ·
    <a href="https://git.thm.de/lgil88/tpl/-/issues">Request Feature</a>
  </p>
</div>

## About TPL

TPL is a language that is translated to C++ template metaprograms by its compiler.
It simplifies creation of template metaprograms and showcases the computability abilities of the C++ template system.
The compiler is implemented in [Scala](https://www.scala-lang.org/).

## Supported Compilers

In theory TPL should support any Compiler that implements the C++ 17 ISO Standard correctly.
In practice, most compiler don't do that, not even in conformance mode.
The code the TPL compiler generates is tested to work on `g++-11` with

```g++-11 -ftemplate-depth=100000 -pedantic -Werror -std=c++17```

Even g++ 11 [does not implement C++ 17 entirely correct](https://gcc.gnu.org/bugzilla/show_bug.cgi?id=85282).
TPL uses workarounds for these issues.

## Getting Started

To use this project you first need to compile the compiler.

### Prerequisites

To use TPL you need a working java installation.
To build the TPL Compiler you also need Scala and SBT.
Installation directions can be found [here](https://www.scala-sbt.org/1.x/docs/Installing-sbt-on-Linux.html).
Alternatively you can use [IntelliJ](https://www.jetbrains.com/idea/) to play around with it.

### Building the Compiler

This can be done by executing

```shell
sbt assembly
```

in the root directly of this project.
The compiler can then be found at `target/scala-2.13/tpl.jar`

### Using the Compiler

The compiler can be executed with a java installation.
It has the following options:

```
Usage: 'java -jar tpl.jar' [OPTIONS...]

Executes all compiler phases up to (and including) the specified one.
If no flag is specified, all phases are run and code is written to the output file.
Options:
  --in PATH                Add a folder or a file to the list of input files. Can be used any number of times.
  --out PATH               Specifies the path of the output folder. 
                           All generated files will be placed there.
  --parse                  Check just for syntactic correctness.
  --absyn                  Check for syntactic correctness and output the abstract syntax tree.
  --disable-optimization   Disables an optimization that reduces code size.
```

### Running tests
There are a number of integration tests that check the correctness of the implementation.
They can be started by running

```shell
sbt test
```

New tests can be added [easily](#contributing).

### Using the library

This repository also contains a (admittedly small) library with some general purpose TPL Code.
The folder `stdlib` can be added as an input folder with `--in` to any compiler invocation to allow your code to use the library.

## Contributing

Any contributions you make are **greatly appreciated**.
The project currently benefits the most from additional test cases.
Tests are dynamically loaded from test files in the `tests` folder.
Add a `*.tpl` File or a folder containing a `test.tpl` file for tests that need more than one source file.
You can also supply a comment to the test file (or the `test.tpl` for folders) to specify an expected outcome:

```
// name: <any text> -- The name of the test case
// stdlib: <true|false> -- Whether to include the standard library when compiling this test
// outcome: <compilererror|compilersuccess|runtimeerror|success> -- The expected outcome when compiling/running this test case
// output: <any text> -- The compiler output for outcome=compilersuccess or the output of the executed program if outcome=success
```

Test cases are run with `g++ 11` when `outcome` is either `runtimeerror` or `success`.
A main function is included with `src/test/ressources/test_main.cpp` to prettify computed results and output them when `outcome` is `success`.

## Contact

Lukas Gail - lukas.gail@mni.thm.de

Project Link: [https://git.thm.de/lgil88/cpp-template-language](https://git.thm.de/lgil88/tpl)
