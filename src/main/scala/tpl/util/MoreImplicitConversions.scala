package tpl.util

import scala.util.parsing.combinator.{ImplicitConversions, Parsers}

trait MoreImplicitConversions extends ImplicitConversions {
  self: Parsers =>

  implicit def flatten6[R, A, B, C, D, E, F](action: (A, B, C, D, E, F) => R): ~[~[~[~[~[A, B], C], D], E], F] => R = {
    case a ~ b ~ c ~ d ~ e ~ f => action(a, b, c, d, e, f)
  }

  implicit def flatten7[R, A, B, C, D, E, F, G](action: (A, B, C, D, E, F, G) => R): ~[~[~[~[~[~[A, B], C], D], E], F], G] => R = {
    case a ~ b ~ c ~ d ~ e ~ f ~ g => action(a, b, c, d, e, f, g)
  }
}
