package tpl.util

import tpl.Types
import tpl.Types.Variance

object ErrorFormats {
  
  def adjective(variance: Variance): String = variance match {
    case Types.CoVariance => "covariant"
    case Types.ContraVariance => "contravariant"
    case Types.InVariance => "invariant"
  }

}
