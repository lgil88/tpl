package tpl

import tpl.absyn.Definition.{FunctionDeclaration, FunctionDefinition, MemberDeclaration, ModuleLevelDefinition, Parameter, TypeParameter}
import tpl.absyn.{Definition, Info}
import tpl.phases._03_semantic.SemanticAnalysis

import scala.collection.MapView
import scala.util.chaining.scalaUtilChainingOps

object Types {
  sealed trait Type {
    def instantiate(args: List[Type]): Type = {
      if (args.isEmpty) return this // Ignore 

      this match {
        case g@GenericType(parameters, underlying) =>
          val replacements = (parameters zip args).toMap

          replace(replacements)(underlying) match {
            case c: TplClassType => c.copy(instance = Some(InstanceInfo(g, replacements)))
            case a => a
          }
        case _ => throw TPLError.TypeDoesNotTakeParameter(this)
      }
    }

    override def toString: String = this match {
      case TplTypeVariable(typeParameter /*, bound*/) => typeParameter.name
      case GenericType(parameters, underlying) => s"[${
        parameters.map({
          case TplTypeVariable(n /*, AnyType*/) => n.name
          //case TypeVariable2(n, b) => s"${n.name} <: $b"
        }).mkString(", ")
      }] -> $underlying"
      case AnyType => "Any"
      case TplClassType(id, _, _, _, _, _) => s"${id.identity.name}${if (id.parameters.isEmpty) "" else id.parameters.mkString("[", ", ", "]")}"
      case FunctionType(parameterTypes, resultType) => s"(${parameterTypes.mkString(", ")}) => $resultType"
      case TupleType(parameterTypes) => s"(${parameterTypes.mkString(", ")})"
      case NothingType => "Nothing"
    }
  }

  case class TplTypeVariable(typeParameter: TypeParameter /*, upperBound: Type*/) extends Type {
    var origin: Info[TypeParameter] = new Info()

    override def equals(obj: Any): Boolean = obj match {
      case obj: TplTypeVariable => this.typeParameter eq obj.typeParameter
      case _ => false
    }
  }

  case class GenericType(parameters: List[TplTypeVariable], underlying: Type) extends Type
  sealed trait Variance {
    def opposite: Variance = this match {
      case CoVariance => ContraVariance
      case ContraVariance => CoVariance
      case InVariance => InVariance
    }
    def compatible(to: Variance): Boolean = (this, to) match {
      case (_, InVariance) => true
      case (InVariance, _) => true
      case (a, b) if a == b => true
      case _ => false
    }
  }

  case object CoVariance extends Variance
  case object ContraVariance extends Variance
  case object InVariance extends Variance

  /**
   * The ClassId is used to implement named type equality over structural type equality.
   * It contains an object whose reference is the identity of the class. Only if the identity is equal two classes can be equal.
   * It also contains the functionality for co-, contra- and invariance regarding a class's type parameters.
   *
   * @param identity   An identity object for a class. Unique for every class.
   * @param variances  The list of variances of the class's potential type parameters, in order.
   * @param parameters The list of actual type parameters. Contains only TypeVariables if the class is still in its generic form.
   */
  case class TplClassId(identity: ModuleLevelDefinition, variances: List[Variance], parameters: List[Type]) {
    /**
     * Checks whether two class ids originate from the same class definition
     *
     * @param other
     * @return
     */
    def <>(other: TplClassId): Boolean = this.identity eq other.identity

    override def equals(other: Any): Boolean = other match {
      case other: TplClassId => (this <> other) && ((this.parameters zip other.parameters) forall Function.tupled(_ == _))
      case _ => false
    }
  }

  case class InstanceInfo(generic: GenericType, boundParameters: Map[TplTypeVariable, Type])

  case class MethodType(t: Type, isAbstract: Boolean, definition: Definition)

  case object AnyType extends Type

  sealed trait MethodSource
  case class DefinitionSource(definition: Definition) extends MethodSource
  case class InheritanceSource(`class`: TplClassType) extends MethodSource

  case class TplClassType(id: TplClassId,
                          baseClass: Option[TplClassType],
                          constructorParams: Option[List[Parameter]],
                          methodDefinitions: Map[String, MemberDeclaration],
                          instance: Option[InstanceInfo],
                          isAbstract: Boolean
                       ) extends Type {

    final def isTrait: Boolean = isAbstract
    final def isRegularClass: Boolean = constructorParams.isDefined
    final def isSingletonObject: Boolean = constructorParams.isEmpty && !isAbstract

    def baseClassHierarchy: List[TplClassType] = this :: baseClass.map(_.baseClassHierarchy).getOrElse(Nil)

    def superClass: Type = replacer(baseClass.getOrElse(AnyType))

    private def replacer(t: Type): Type = instance.map(i => replace(i.boundParameters) _).getOrElse(identity[Type] _)(t)

    def constructor: Option[FunctionType] = constructorTypes.map(p => FunctionType(p, this))
    def constructorTypes: Option[List[Type]] = constructorParams.map(_.tapEach(SemanticAnalysis.analyze).map(_.type_info())).map(_.map(replacer))

    def access(id: String, typeParameters: List[Type]): Type = {
      // First option: a constructor parameter if applicable
      val cs_param = constructorParams.flatMap(_.find(_.name == id).map(_.typ.type_info()))
      // Second option: Any other defined member
      val other_members = this.methodTypes.get(id).map(_.t)

      // Apply type parameters if applicable
      cs_param.orElse(other_members).map({
        case _: GenericType if typeParameters.isEmpty => throw TPLError.MemberNeedsTypeParameters(id)
        case t: GenericType => t.instantiate(typeParameters)
        case _ if typeParameters.nonEmpty => throw TPLError.MemberDoesNotTakeTypeParameters(id)
        case t => t
      })
              .map(replacer) // Apply replacement of generic parameters
              .orElse(baseClass.map(_.access(id, typeParameters))) // if no fitting member exists in this class, go to the base class
              .getOrElse(throw TPLError.NoSuchMember(id)) //
    }

    override def equals(other: Any): Boolean = other match {
      case other: TplClassType => this.id == other.id // For classes we have named type equality and not structural type equality. This functionality is delegated to the ClassId object.
      case _ => false
    }

    def methodTypes: MapView[String, MethodType] = {
      // Ensure methods are analyzed
      this.methodDefinitions.view.mapValues(_.tap(SemanticAnalysis.analyze)).mapValues({
        case f: FunctionDefinition =>
          MethodType(replacer(f.type_info()), isAbstract = false, f)
        case f: FunctionDeclaration =>
          MethodType(replacer(f.type_info()), isAbstract = true, f)
      })
    }

    def allMethodTypes: Map[String, MethodType] = baseClass.map(_.allMethodTypes).getOrElse(Map()) ++ methodTypes

    def abstractMethods: Map[String, MethodType] = this.allMethodTypes.filter(_._2.isAbstract)

    def methodSources: Map[String, MethodSource] = baseClass.map(base => base.methodSources.view.mapValues({
      case DefinitionSource(_) => InheritanceSource(base)
      case s: InheritanceSource => s
    }).toMap).getOrElse(Map()) ++ this.methodDefinitions.view.mapValues(DefinitionSource)
  }

  case class FunctionType(parameterTypes: List[Type], resultType: Type) extends Type{
    def arity: Int = parameterTypes.size
  }
  case class TupleType(parameterTypes: List[Type]) extends Type {
    def arity: Int = parameterTypes.size
  }
  case object NothingType extends Type

  object TypeOps {

    import tpl.Types.TypeSyntax

    // TODO: Move functions from below

    /**
     * The union of two types is a type that both types are compatible to.
     * In other words in finds a common base class.
     * It unifies two types towards 'Any'.
     *
     * @return The unified type
     */
    def ∪(t1: Type, t2: Type): Type = {
      (t1, t2) match {
        case (a, b) if a ⊆ b => b
        case (a, b) if b ⊆ a => a
        //case (TypeVariable2(_/*, upperBound*/), b) => upperBound ∪ b
        //case (a, TypeVariable2(_/*, upperBound*/)) => a ∪ upperBound
        case (FunctionType(params1, result1), FunctionType(params2, result2)) if params1.size == params2.size => FunctionType((params1 zip params2) map Function.tupled(_ ∩ _), result1 ∪ result2) // Use contravariance for parameters
        case (TupleType(params1), TupleType(params2)) if params1.size == params2.size => TupleType((params1 zip params2) map Function.tupled(_ ∪ _)) // Tuple types are straightforward and just a union over their components
        case (self: TplClassType, other: TplClassType) if self.id <> other.id =>
          def unionVarianceList(variances: List[Variance], a: List[Type], b: List[Type]): Option[List[Type]] = {
            val res = variances zip (a zip b) map { case (va, (a, b)) => this.∪(va)(a, b) }

            Option.when(!res.exists(_.isEmpty))(res.map(_.get))
          }

          def unionClassParameterList(first: TplClassType, second: TplClassType): Type = {
            unionVarianceList(second.id.variances, first.id.parameters, second.id.parameters).flatMap(t => second.instance.map(_.generic.instantiate(t))).getOrElse(first.superClass ∪ second.superClass)
          }

          if (self.instance.isDefined) unionClassParameterList(self, other) else self
        case (self: TplClassType, other: TplClassType) =>
          val selfSequence = self.baseClassHierarchy
          val otherSequence = other.baseClassHierarchy

          // Find the least class that appears in both supertype sequences
          val leastCommonSuperClass = selfSequence.view.flatMap(s => otherSequence.find(_.id <> s.id).map((s, _))).headOption

          // Apply the union operation on the type arguments taking variances into consideration
          leastCommonSuperClass.map(Function.tupled(_ ∪ _)).getOrElse(AnyType)

        /*

        val selfErased = self.erased
        val otherErased = other.erased

        if (selfErased ⊆ otherErased) unionClassParameterList(self, other) // self is compatible to other
        else if (otherErased ⊆ selfErased) unionClassParameterList(other, self) // other is compatible to self
        else self.baseType ∪ other.baseType // Neither is compatible to the other, go up 1 level to their base classes and repeat
        */
        case _ => AnyType
      }
    }

    def ∪(variance: Variance)(a: Type, b: Type): Option[Type] = variance match {
      case CoVariance => Some(a ∪ b)
      case ContraVariance => Some(a ∩ b)
      case InVariance if a == b => Some(a)
      case InVariance => None
    }

    /**
     * The intersection of two types is a type that is compatible to both types.
     * In other words in finds a common subclass.
     * It unifies two types towards 'Nothing'.
     *
     * @return The unified type
     */
    def ∩(self: Type, other: Type): Type = {
      (self, other) match {
        case (a, b) if a ⊆ b => a
        case (a, b) if b ⊆ a => b
        //case (TypeVariable2(_, upperBound), b) => upperBound ∩ b
        //case (a, TypeVariable2(_, upperBound)) => a ∩ upperBound
        case (FunctionType(params1, result1), FunctionType(params2, result2)) if params1.size == params2.size => FunctionType((params1 zip params2) map Function.tupled(_ ∪ _), result1 ∩ result2) // Use contravariance for parameters
        case (TupleType(params1), TupleType(params2)) if params1.size == params2.size => TupleType((params1 zip params2) map Function.tupled(_ ∩ _)) // Tuple types are straightforward and just a union over their components
        case (self: TplClassType, other: TplClassType) if self.id == other.id =>
          def intersectVarianceList(variances: List[Variance], a: List[Type], b: List[Type]): Option[List[Type]] = {
            val res = variances zip (a zip b) map { case (va, (a, b)) => this.∩(va)(a, b) }

            Option.when(!res.exists(_.isEmpty))(res.map(_.get))
          }

          def intersectClassParameterList(lower: TplClassType, upper: TplClassType): Type = intersectVarianceList(lower.id.variances, lower.id.parameters, upper.id.parameters).flatMap(t => lower.instance.map(_.generic.instantiate(t))).getOrElse(NothingType)

          // Sooo.... This is pretty hacky and I am not sure if this is the best one could do.
          // The problem:
          // class Contra[-T]
          //
          // trait Seq[+T]
          // trait List[+T] extends Seq[T]
          // trait Base
          // trait Derived extends Base
          //
          // What's the intersection type of Seq[Derived] and List[Base]?
          // A sensible answer would probably be List[Derived], because it definitely is both a Seq[Derived] and a List[Base].
          // But there could exist other types that fulfill this requirement too.
          // Scala deduces 'Seq[Derived] with List[Base]', which is obviously correct.
          // There is no way to explicitly name intersection types like this in TPL, so the only nameable type I can use is Nothing.
          // I can only do better if both operands of the intersection are instances of the same type constructor or are in a direct subtype relation to each other.

          intersectClassParameterList(self, other)

        /*

        val selfSequence = self.baseClassSequence
        val otherSequence = other.baseClassSequence

        val LeastCommonSuperClass = selfSequence.view.flatMap(s => otherSequence.find(_.id == s.id).map((s, _))).headOption

        return LeastCommonSuperClass match {
          case Some((a, b)) => intersectClassParameterList(a, b)
          case None => self.baseType ∪ other.baseType
        }*/

        /*
        val selfErased = self.erased
        val otherErased = other.erased

        if (selfErased ⊆ otherErased) intersectClassParameterList(self, other) // self is compatible to other
        else if (otherErased ⊆ selfErased) intersectClassParameterList(other, self) // other is compatible to self
        else NothingType // Neither is compatible to the other. There is no multiple inheritance, so the only possible type is the NothingType
         */
        case _ => NothingType
      }
    }

    def ∩(variance: Variance)(a: Type, b: Type): Option[Type] = variance match {
      case CoVariance => Some(a ∩ b)
      case ContraVariance => Some(a ∪ b)
      case InVariance if a == b => Some(a)
      case InVariance => None
    }

    /**
     * Checks whether a type is compatible to another type.
     * In other words: Returns true if a value of the first type can be passed to a parameter of the second type.
     */
    def ⊆(self: Type, other: Type): Boolean = {
      (self, other) match {
        case (_, AnyType) => true
        case (NothingType, _) => true
        case (a, b) if a == b => true // Identical types are obviously always compatible
        //case (TypeVariable2(_, upperBound), b) => upperBound ⊆ b
        case (FunctionType(params1, result1), FunctionType(params2, result2)) => (params1.size == params2.size) && ((params2 zip params1) forall Function.tupled(_ ⊆ _)) && (result1 ⊆ result2)
        case (TupleType(params1), TupleType(params2)) => (params1.size == params2.size) && ((params1 zip params2) forall Function.tupled(_ ⊆ _))
        case (self: TplClassType, other: TplClassType) =>
          // Whether the classes are instances of the same generic class
          val identical = self.id <> other.id

          // Whether the (optional) type parameters fulfill the set variance rules. Chained to 'identical' with '&&' to avoid crashes due to zip with different list lengths.
          val compatible = identical && ((self.id.variances zip (self.id.parameters zip other.id.parameters)) forall {
            case (CoVariance, (a, b)) => a ⊆ b
            case (ContraVariance, (a, b)) => b ⊆ a
            case (InVariance, (a, b)) => a == b
          })

          compatible || (self.superClass ⊆ other)
        case (a: GenericType, b: GenericType) =>
          // Okay, this is a little weird, so listen closely.
          // This implementation is needed to check whether a generic method is a correct override for another generic method
          // Type parameters for generic functions can have upper type bounds, but NOT variances.
          // Thus, variances are completely ignored here!!

          (a.parameters.size == b.parameters.size) && // A method can only be a correct override if the number of type parameters match
            // a.parameters.zip(b.parameters).forall({ case (overriding, overridden) => overridden.upperBound ⊆ overriding.upperBound }) && // The type bounds of the overriding method can't be stricter than the type bounds in the overridden method. They can be less strict
            (a.instantiate(b.parameters) ⊆ b.underlying) // The generic type of the overriding method is now instantiated with the type parameters of the overridden method. This matches all type parameters with their pendant in the other method.

        case _ => false
      }
    }

    /*
    @tailrec
    def upperBound(t: Type): Type = t match {
      case TypeVariable2(_, upperBound) => TypeOps.upperBound(upperBound)
      case t => t
    }*/

    def underlying(t: Type): Type = t match {
      case GenericType(_, underlying) => underlying
      case t => t
    }
  }

  implicit class TypeSyntax(self: Type) {
    def ∪(other: Type): Type = TypeOps.∪(self, other)
    def ∩(other: Type): Type = TypeOps.∩(self, other)
    def ⊆(other: Type): Boolean = TypeOps.⊆(self, other)
    def ⊈(other: Type): Boolean = !(self ⊆ other)

    //def bound: Type = TypeOps.upperBound(self)
    def underlying: Type = TypeOps.underlying(self)
  }

  def replace(replacements: Map[TplTypeVariable, Type])(t: Type): Type = {
    val replacer = replace(replacements) _

    t match {
      case variable: TplTypeVariable => replacements.getOrElse(variable, variable /*, variable.copy(upperBound = replacer(variable.upperBound))*/)
      case GenericType(parameters, underlying) => GenericType(parameters.map(replacer).map(_.asInstanceOf[TplTypeVariable]), replacer(underlying))
      case c: TplClassType => c.copy(
        id = c.id.copy(parameters = c.id.parameters.map(replacer)),
        baseClass = c.baseClass.map(replacer).map(_.asInstanceOf[TplClassType]),
        instance = c.instance.map(i => InstanceInfo(i.generic, i.boundParameters.view.mapValues(replacer).toMap)))
      case FunctionType(parameterTypes, resultType) => FunctionType(parameterTypes.map(replacer), replacer(resultType))
      case TupleType(parameterTypes) => TupleType(parameterTypes.map(replacer))
      case AnyType => AnyType
      case NothingType => NothingType
    }
  }
}
