package tpl

import tpl.Types.{Type, Variance}

import scala.util.parsing.input.{NoPosition, Position}

case class TPLError(msg: String) extends Throwable {
  var pos: Position = NoPosition

  override def toString: String = {
    if (pos == NoPosition) s"error: $msg"
    else s"$pos: error: $msg"
  }
}
object TPLError {

  import tpl.util.ErrorFormats._

  def SyntaxError(msg: String) = TPLError(s"Syntax Error: $msg")

  def CyclicTypeDependency = TPLError("Cyclical type dependency found")

  def IllegalIdentifierCppKeyword(i: String) = TPLError(s"Illegal identifier '$i'. '$i' is a keyword in C++.")
  def IllegalIdentifierReserved(i: String) = TPLError(s"Illegal identifier '$i'. '$i' is a reserved identifier for code generation purposes.")

  def AccessNoneClassType = TPLError("Accessing a member on a non class type")
  def NoSuchMember(m: String) = TPLError(s"No such member '$m'")
  def UndefinedName(name: String): TPLError = TPLError(s"Undefined name $name")
  def UndefinedName(name: absyn.QualifiedName): TPLError = TPLError(s"Undefined name $name")
  def WrongNumberOfArguments(): TPLError = TPLError(s"Wrong number of arguments")
  def CallOfNonFunction(): TPLError = TPLError("Call of non-function")
  def TypeMismatch(expected: Type, got: Type): TPLError = TPLError(s"Type mismatch. Expected: $expected, got $got")
  def MemberDoesNotTakeTypeParameters(name: String): TPLError = TPLError(s"Member $name does not take type parameters")
  def MemberNeedsTypeParameters(name: String): TPLError = TPLError(s"Member $name needs type parameters.")
  def ModuleDoesNotTakeTypeParameters(name: String): TPLError = TPLError(s"Module $name does not take type parameters.")
  def TypeDoesNotTakeParameter(`type`: Type): TPLError = TPLError(s"Type ${`type`} does not take type parameters.")
  def NotAClassType(`type`: Type): TPLError = TPLError(s"Type ${`type`} is not a class type.")
  def IllegalInheritanceNonAbstractClass(`type`: Type): TPLError = TPLError(s"Illegal inheritance from non abstract type ${`type`}.")
  def NotAType(name: absyn.QualifiedName): TPLError = TPLError(s"${`name`} is not a type.")
  def NotAModule(name: String): TPLError = TPLError(s"${`name`} is not a module.")
  def NotAValue(name: absyn.QualifiedName): TPLError = TPLError(s"${name.qualifier.mkString(".")} does not refer to a value.")
  def VarianceMismatch(expected: Variance, actual: Variance, typ_name: String) = TPLError(s"${adjective(actual).capitalize} $typ_name at ${adjective(expected)} position")
  def IllegalOverride(name: String, base: Type, actual: Type) = TPLError(s"Method type $actual does not override $base for method $name")
  def MultiplyDefined(name: String, definedAt: Position, alreadyDefinedAt: Position) = TPLError(s"Name '$name' defined at $definedAt is already defined at $alreadyDefinedAt")
  def ImportedNameAlreadyDefined(name: String, importedAt: Position, alreadyDefinedAt: Position) = TPLError(s"Name '$name' imported at $importedAt is being defined at $alreadyDefinedAt")
  def ObjectPatternDoesNotNameObject(name: absyn.QualifiedName) = TPLError(s"Name '$name' does not name a value")
  def InvalidExpressionInEqualityPattern = TPLError(s"Invalid expression in Equality Pattern. Only qualified names are supported as of right now.")
}
