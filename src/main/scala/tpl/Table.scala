package tpl

import tpl.Types.Type
import tpl.absyn.Definition.ImportDefinition
import tpl.absyn.{Definition, Info}
import tpl.phases._03_semantic.SemanticAnalysis
import tpl.phases._05_codegen.Constants

import scala.collection.mutable
import scala.util.chaining.scalaUtilChainingOps
import scala.util.parsing.input.Position

object Table {
  sealed trait SymbolEntry
  case class DefinitionEntry(definition: Definition) extends SymbolEntry
  case class ModuleEntry(module: absyn.Module) extends SymbolEntry

  class SymbolTable(val parent: Option[SymbolTable], isModule: Boolean,
                    definitions: mutable.Map[String, Definition],
                    submodules: mutable.Map[String, absyn.Module],
                    imports: mutable.Map[String, SymbolEntry],
                   ) {

    def isRootTable: Boolean = this.parent.isEmpty || this.parent.get.parent.isEmpty

    val thisType: Info[Type] = new Info[Type]

    def resolvedThis: Option[Type] = if (this.thisType.hasValue) Some(this.thisType()) else this.parent.flatMap(_.resolvedThis)

    def this(rootModule: absyn.Module) = {
      this(None, true, mutable.Map(), mutable.Map(), mutable.Map())

      this.submodules.put("__root__", rootModule)
    }
    def this(parent: SymbolTable, isModule: Boolean) = this(Some(parent), isModule, mutable.Map(), mutable.Map(), mutable.Map())

    private def alreadyDefinedAt(name: String): Option[Position] = definitions.orElse(submodules).lift(name).map(_.position())

    private def checkMultipleDefinition(name: String, position: Position): Unit = alreadyDefinedAt(name).foreach(alr => throw TPLError.MultiplyDefined(name, position, alr))

    def declare(definition: Definition): Unit = {
      checkMultipleDefinition(definition.name, definition.position())
      definitions.put(definition.name, definition)
    }
    def declare(module: absyn.Module): Unit = {
      checkMultipleDefinition(module.name, module.position())
      submodules.put(module.path.last, module)
    }
    def declare(import_declaration: ImportDefinition): Unit = {
      definitions.orElse(submodules).lift(import_declaration.name).map(_.position()).foreach(alr => throw TPLError.ImportedNameAlreadyDefined(import_declaration.name, import_declaration.position(), alr))

      
      imports.put(import_declaration.name, lookup(import_declaration.full_path, analyse_if_needed = false))
    }

    private def find(name: String, analyse_if_needed: Boolean): Option[SymbolEntry] = {
      val here = definitions.get(name).map(DefinitionEntry)
                            .orElse(imports.get(name))
                            .orElse(submodules.get(name).map(ModuleEntry))

      here.orElse(this.parent.flatMap(_.find(name, analyse_if_needed))).map(_.tap({
        case DefinitionEntry(definition) if analyse_if_needed => SemanticAnalysis.analyze(definition)
        case _ =>
      }))
    }

    def lookup(name: String, analyse_if_needed: Boolean): SymbolEntry = find(name, analyse_if_needed).getOrElse(throw TPLError.UndefinedName(name))

    def lookup(name: absyn.QualifiedName, analyse_if_needed: Boolean): SymbolEntry = {
      def get_module(qualifier: List[String]): absyn.Module = {
        qualifier match {
          case root :: Nil => lookup(root, analyse_if_needed) match {
            case ModuleEntry(module) => module
            case _ => throw TPLError.NotAModule(root)
          }
          case head :: tail => get_module(tail).names(head) match {
            case Left(value) => value
            case _ => throw TPLError.NotAModule(head)
          }
        }
      }

      name.qualifier.reverse match {
        case id :: Nil => lookup(id, analyse_if_needed)
        case id :: qualifier_names => get_module(qualifier_names).names(id) match {
          case Left(value) => ModuleEntry(value)
          case Right(value) =>
            if (analyse_if_needed) SemanticAnalysis.analyze(value)
            DefinitionEntry(value)
        }
      }
    }

    def level: Int = this.parent.map(_.level + 1).getOrElse(0)
    def scopeName: String = Constants.scope_parameter_name(level)
  }
}
