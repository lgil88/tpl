package tpl

import tpl.Table.SymbolTable
import tpl.Types.Type
import tpl.absyn.Definition.{ImportDefinition, ModuleLevelDefinition}

import scala.util.parsing.input.{NoPosition, Position}

package object absyn {
  class Info[T] {
    var value: Option[T] = None
    var being_calculated: Boolean = false

    def this(initialValue: T) = {
      this()
      this.set(initialValue)
    }

    def beginCalculation(): Unit = this.being_calculated = true
    def hasValue: Boolean = this.value.isDefined
    def set(value: T): Unit = {
      this.value = Some(value)
      this.being_calculated = false
    }

    def :=(value: T): Unit = this.set(value)

    def apply(): T = value.get
  }

  trait WithContext {
    val context = new Info[SymbolTable]
  }

  trait WithType {
    val type_info = new Info[Type]
  }

  trait OpensScope {
    val scope = new Info[SymbolTable]
  }

  trait WithPosition {
    val position = new Info[Position](NoPosition)
  }

  case class Module(path: List[String], imports: List[ImportDefinition], definitions: List[ModuleLevelDefinition]) extends WithContext with OpensScope with WithPosition {
    val submodules = new Info[List[Module]]

    val path_with_root = "__root__" :: path

    val name = path_with_root.last

    def names(name: String): Either[Module, Definition] = definitions.find(_.name == name).map(Right(_)).orElse(submodules().find(_.path.last == name).map(Left(_))).getOrElse(throw TPLError.NoSuchMember(name))
  }

  case class Program(modules: List[Module]) {
    val root_module = new Info[Module]
  }
  case class Name(identifier: String, type_parameters: List[TypeExpression]) extends WithPosition
  case class QualifiedName(qualifier: List[String], type_parameters: List[TypeExpression]) extends WithPosition
}
