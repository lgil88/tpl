package tpl.absyn

import tpl.absyn.Definition.ValueDefinition

sealed trait Pattern extends WithPosition with WithContext with WithType {
  import Pattern._
  
  lazy val freeVariables: List[NamedPattern] = this match {
    case TuplePattern(patterns) => patterns.flatMap(_.freeVariables)
    case ValuePattern(_, argumentPatterns) => argumentPatterns.flatMap(_.freeVariables)
    case ObjectPattern(_) => Nil
    case p: NamedPattern => List(p)
    case WildcardPattern() => Nil
  }
  
  lazy val usedPatternNames: List[TypeExpression] = this match {
    case TuplePattern(patterns) => patterns.flatMap(_.usedPatternNames)
    case ValuePattern(name, argumentPatterns) => name :: argumentPatterns.flatMap(_.usedPatternNames)
    case ObjectPattern(_) => Nil
    case NamedPattern(_) => Nil
    case WildcardPattern() => Nil
  }

  lazy val wildcardCount: Int = this match {
    case TuplePattern(patterns) => patterns.map(_.wildcardCount).sum
    case ValuePattern(_, argumentPatterns) => argumentPatterns.map(_.wildcardCount).sum
    case ObjectPattern(_) => 0
    case NamedPattern(_) => 0
    case WildcardPattern() => 1
  }
}

object Pattern {
  case class TuplePattern(patterns: List[Pattern]) extends Pattern
  case class ValuePattern(name: TypeExpression, argumentPatterns: List[Pattern]) extends Pattern
  case class ObjectPattern(name: Expression) extends Pattern
  case class NamedPattern(name: String) extends Pattern with ValueDefinition
  case class WildcardPattern() extends Pattern
}