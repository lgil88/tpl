package tpl.absyn

import tpl.Types.{Type, Variance}
import tpl.absyn

sealed trait TypeExpression extends WithContext with WithPosition {
  val type_info = new Info[Type]
  val expected_variance = new Info[Variance]
}

object TypeExpression {
  case class NamedTypeExpression(name: absyn.QualifiedName) extends TypeExpression
  case class FunctionTypeExpression(params: List[TypeExpression], result: TypeExpression) extends TypeExpression
  case class TupleTypeExpression(parts: List[TypeExpression]) extends TypeExpression

  case class AnyTypeExpression() extends TypeExpression
  case class NothingTypeExpression() extends TypeExpression
}
