package tpl.absyn

import tpl.Types.Type
import tpl.absyn.Definition.{ImportDefinition, LocalDefinition, Parameter}


sealed trait Expression extends WithPosition with WithContext {
  val type_info = new Info[Type]
}

object Expression {
  case class MatchCase(pattern: Pattern, body: Expression) extends WithPosition with OpensScope

  case class TupleExpression(elements: List[Expression]) extends Expression
  case class ThisExpression() extends Expression
  case class NamedExpression(name: Name) extends Expression {
    val qualified_name = new Info[QualifiedName]
  }
  case class MemberAccess(obj: Expression, name: Name) extends Expression { // abc.member, abc.member[C] 
    val qualified_name = new Info[QualifiedName]
  }

  case class LambdaExpression(params: List[Parameter], resultType: Option[TypeExpression], body: Expression) extends Expression
  case class FunctionApplication(callee: Expression, arguments: List[Expression]) extends Expression
  case class CompoundExpression(imports: List[ImportDefinition], localDefinitions: List[LocalDefinition], result: Expression) extends Expression with OpensScope
  case class MatchExpression(matchee: Expression, cases: List[MatchCase]) extends Expression
  case class TypedExpression(base: Expression, typ: TypeExpression) extends Expression

  object QualifiedNamedExpression {
    def unapply(exp: Expression): Option[QualifiedName] = {
      exp match {
        case e: Expression.NamedExpression => e.qualified_name.value
        case e: Expression.MemberAccess => e.qualified_name.value
        case _ => None
      }
    }
  }
}
