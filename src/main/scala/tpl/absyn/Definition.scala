package tpl.absyn

import tpl.Types.Variance
import tpl.phases._04_varalloc.Location

sealed trait Definition extends Declaration with WithContext with WithType with WithPosition {
  val location = new Info[Location]
  def definesClass: Boolean = this match {
    case _: Definition.TraitDefinition | _: Definition.ObjectDefinition | _: Definition.ClassDefinition => true
    case _ => false
  }
}

object Definition {
  sealed trait ModuleLevelDefinition extends Definition {
    val qualified_name = new Info[QualifiedName]
  }
  sealed trait MemberDeclaration extends Definition
  sealed trait MemberDefinition extends MemberDeclaration
  sealed trait LocalDefinition extends Definition with ModuleLevelDefinition

  sealed trait TypeDefinition extends Definition
  trait ValueDefinition extends Definition

  case class TypeParameter(variance: Variance, name: String /*, upperBound: Option[TypeExpression]*/) extends TypeDefinition
  case class Parameter(name: String, typ: TypeExpression) extends ValueDefinition

  case class ValDefinition(name: String, typ: Option[TypeExpression], value: Expression) extends LocalDefinition with ValueDefinition

  case class TraitDefinition(name: String, typeParameters: List[TypeParameter], extension: Option[TypeExpression], members: List[MemberDeclaration]) extends ModuleLevelDefinition with TypeDefinition
  case class ClassDefinition(name: String, typeParameters: List[TypeParameter], parameters: List[Parameter], extension: Option[TypeExpression], members: List[MemberDefinition]) extends ModuleLevelDefinition with TypeDefinition with ValueDefinition
  case class ObjectDefinition(name: String, extension: Option[TypeExpression], members: List[MemberDefinition]) extends ModuleLevelDefinition with ValueDefinition

  case class FunctionDeclaration(name: String, typeParameters: List[TypeParameter], parameters: List[Parameter], resultType: TypeExpression) extends MemberDeclaration with ValueDefinition
  case class FunctionDefinition(name: String, typeParameters: List[TypeParameter], parameters: List[Parameter], resultType: TypeExpression, body: Expression) extends LocalDefinition with MemberDefinition with ValueDefinition

  case class ImportDefinition(qualified_name: List[String]) extends WithPosition {
    val name: String = qualified_name.last
    
    val full_path = QualifiedName("__root__" :: qualified_name, Nil)
  }
}