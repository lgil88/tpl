package tpl.phases._01_scanner

object Tokens {
  sealed trait TokenClass

  case object IDENT extends TokenClass

  sealed trait Delimiter extends TokenClass
  case object LPAREN extends TokenClass
  case object RPAREN extends TokenClass
  case object LBRACK extends TokenClass
  case object RBRACK extends TokenClass
  case object LCURL extends TokenClass
  case object RCURL extends TokenClass
  
  case object ARROW extends TokenClass
  case object UNDERSCORE extends TokenClass
  case object COMMA extends TokenClass
  case object COLON extends TokenClass
  case object EQUALS extends TokenClass
  case object PERIOD extends TokenClass
  case object PLUS extends TokenClass
  case object MINUS extends TokenClass

  sealed trait Keyword extends TokenClass
  case object EXTENDS extends Keyword
  case object MODULE extends Keyword
  case object TRAIT extends Keyword
  case object TUPLE extends Keyword
  case object CLASS extends Keyword
  case object OBJECT extends Keyword
  case object DEF extends Keyword
  case object VAL extends Keyword
  case object MATCH extends Keyword
  case object CASE extends Keyword
  case object NOTHING extends Keyword
  case object ANY extends Keyword
  case object THIS extends Keyword
  
  // The following tokens are implemented and reserved, but their associated feature are not implemented fully (yet)
  case object LAZY extends Keyword
  case object LTCOLON extends TokenClass
  case object GTCOLON extends TokenClass
  case object IMPORT extends TokenClass
}

