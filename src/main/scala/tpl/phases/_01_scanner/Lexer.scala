package tpl.phases._01_scanner

import tpl.TPLError
import tpl.phases._01_scanner.Tokens.TokenClass
import tpl.phases._03_semantic.SemanticAnalysis.AtPosition
import tpl.phases._05_codegen.Constants
import tpl.util.{FileReader, ScannerUtilities, TokenReader}

import java.nio.file.Path
import scala.util.parsing.input._

object Lexer {
  object LexicalGrammar extends ScannerUtilities[TokenClass] {

    import Tokens._

    lazy val whitespace: Parser[Any] = """(\s|(//.*)|(/\*[^*]*\*+(?:[^/*][^*]*\*+)*/))*""".r

    val CppKeywords = List("asm", "else", "new", "this", "auto", "enum", "operator", "throw", "bool", "explicit", "private", "true", "break", "export", "protected", "try", "case", "extern", "public", "typedef", "catch", "false", "register", "typeid", "char", "float", "reinterpret_cast", "typename", "class", "for", "return", "union", "const", "friend", "short", "unsigned", "const_cast", "goto", "signed", "using", "continue", "if", "sizeof", "virtual", "default", "inline",
                           "static", "void", "delete", "int", "static_cast", "volatile", "do", "long", "struct", "wchar_t", "double", "mutable", "switch", "while", "dynamic_cast", "namespace", "template")
    
    lazy val token: Parser[Symbol] = (in: Input) =>
      AtPosition(in.pos) {
        {
          ("=>" ^^^ symbol(ARROW) |
            "=" ^^^ symbol(EQUALS) |
            "(" ^^^ symbol(LPAREN) |
            ")" ^^^ symbol(RPAREN) |
            "{" ^^^ symbol(LCURL) |
            "}" ^^^ symbol(RCURL) |
            "[" ^^^ symbol(LBRACK) |
            "]" ^^^ symbol(RBRACK) |
            "," ^^^ symbol(COMMA) |
            ":" ^^^ symbol(COLON) |
            "." ^^^ symbol(PERIOD) |
            "+" ^^^ symbol(PLUS) |
            "-" ^^^ symbol(MINUS) |
            "_" ^^^ symbol(UNDERSCORE) |
            "<:" ^^^ symbol(LTCOLON) |
            """[0-9a-zA-Z]\w*""".r ^^ { // Identifiers can't start with an underscore
              case "module" => symbol(MODULE)
              case "this" => symbol(THIS)
              case "def" => symbol(DEF)
              case "match" => symbol(MATCH)
              case "case" => symbol(CASE)
              case "lazy" => symbol(LAZY)
              case "val" => symbol(VAL)
              case "object" => symbol(OBJECT)
              case "trait" => symbol(TRAIT)
              case "class" => symbol(CLASS)
              case "extends" => symbol(EXTENDS)
              case "tuple" => symbol(TUPLE)
              case "Nothing" => symbol(NOTHING)
              case "Any" => symbol(ANY)
              case "import" => symbol(IMPORT)
              case i if Constants.reservedNames.contains(i) => throw TPLError.IllegalIdentifierReserved(i)
              case i if CppKeywords.contains(i) || i.head.isDigit=>
                symbol(IDENT, "hack_"+i)
                //throw TPLError.IllegalIdentifierCppKeyword(i)
              case i => symbol(IDENT, i)
            }) (in).map(_ (in.pos))
        }
      }
  }

  def apply(path: Path): TokenReader[TokenClass] = TokenReader(FileReader(path), LexicalGrammar)
  def apply(input: String): TokenReader[TokenClass] = TokenReader(new CharSequenceReader(input), LexicalGrammar)
}
