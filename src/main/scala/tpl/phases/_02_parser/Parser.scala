package tpl.phases._02_parser

import tpl.Types.{CoVariance, ContraVariance, InVariance, Variance}
import tpl.absyn.Definition._
import tpl.absyn.Expression._
import tpl.absyn.Pattern._
import tpl.absyn.TypeExpression._
import tpl.absyn._
import tpl.phases._01_scanner.Tokens.TokenClass
import tpl.phases._01_scanner.{Lexer, Tokens}
import tpl.phases._03_semantic.SemanticAnalysis.AtPosition
import tpl.util.{MoreImplicitConversions, ParserUtilities, Token}
import tpl.{TPLError, absyn}

import java.io.File
import java.nio.file.Paths
import scala.util.parsing.input.Reader

object Parser {
  object Grammar extends ParserUtilities[TokenClass] with MoreImplicitConversions {

    def opt_list[T](p: Parser[List[T]]): Parser[List[T]] = opt(p) <^^> (_.toList.flatten)

    import tpl.phases._01_scanner.Tokens._

    import scala.util.chaining.scalaUtilChainingOps

    def ident: Parser[String] = valueToken(IDENT)(classOf[String])

    def name: Parser[Name] = ident ~~ opt_list(LBRACK ~~ rep1sep(type_expression, COMMA) ~~ RBRACK) <^^> Name

    def qualified_name: Parser[QualifiedName] = rep1sep(ident, PERIOD) ~~ opt_list(LBRACK ~~ rep1sep(type_expression, COMMA) ~~ RBRACK) <^^> QualifiedName

    def type_expression: Grammar.Parser[TypeExpression] = type_exp1 | Parser { in => Failure(s"Expected type_expression but got ${in.first}", in) }

    def type_exp1: Parser[TypeExpression] =
      LPAREN ~~ repsep(type_expression, COMMA) ~~ RPAREN ~~ ARROW ~~ type_exp1 <^^> FunctionTypeExpression |
        type_exp2

    def type_exp2: Parser[TypeExpression] =
      TUPLE ~~ LBRACK ~~ repsep(type_expression, COMMA) ~~ RBRACK <^^> TupleTypeExpression |
        qualified_name <^^> NamedTypeExpression |
        ANY <^^> (_ => AnyTypeExpression()) |
        NOTHING <^^> (_ => NothingTypeExpression())

    def function_definition: Grammar.Parser[FunctionDefinition] = DEF ~~ ident ~~ opt_list(type_parameter_list_no_variances) ~~ LPAREN ~~ repsep(parameter, COMMA) ~~ RPAREN ~~ COLON ~~ type_expression ~~ EQUALS ~~ expression <^^> FunctionDefinition
    def function_declaration = DEF ~~ ident ~~ opt_list(type_parameter_list) ~~ LPAREN ~~ repsep(parameter, COMMA) ~~ RPAREN ~~ COLON ~~ type_expression <^^> FunctionDeclaration

    def trait_definition = TRAIT ~~ commit(ident ~~ opt_list(type_parameter_list) ~~ opt(EXTENDS ~~ type_expression) ~~ opt_list(LCURL ~~ rep(function_definition | function_declaration ) ~~ RCURL)) <^^> TraitDefinition
    def class_definition = CLASS ~~ commit(ident ~~ opt_list(type_parameter_list) ~~ LPAREN ~~ repsep(parameter, COMMA) ~~ RPAREN ~~ opt(EXTENDS ~~ type_expression) ~~ opt_list(LCURL ~~ rep(function_definition) ~~ RCURL)) <^^> ClassDefinition
    def object_definition = OBJECT ~~ commit(ident ~~ opt(EXTENDS ~~ type_expression) ~~ opt_list(LCURL ~~ rep(function_definition) ~~ RCURL)) <^^> ObjectDefinition
    def value_definition = VAL ~~ commit(ident ~~ opt(COLON ~~ type_expression) ~~ EQUALS ~~ expression) <^^> ValDefinition

    def module_level_definition: Parser[ModuleLevelDefinition] = function_definition | trait_definition | class_definition | object_definition | value_definition
    def local_definition: Grammar.Parser[LocalDefinition] = function_definition | value_definition
    def import_definition: Grammar.Parser[ImportDefinition] = IMPORT ~~ rep1sep(ident, PERIOD) <^^> ImportDefinition

    def expression: Parser[Expression] = exp1 |
      Parser { in => Failure(s"Expected expression but got ${in.first}", in) }

    def exp1: Parser[Expression] =
      LPAREN ~~ repsep(parameter, COMMA) ~~ RPAREN ~~ opt(COLON ~~ type_expression) ~~ ARROW ~~ exp1 <^^> LambdaExpression | // abstraction, lambda
        exp2


    def exp2: Parser[Expression] = exp3 ~~ opt(MATCH ~~ commit(LCURL ~~ rep(match_case) ~~ RCURL)) <^^> { // exp match { case _ => ... }
      case matchee ~ Some(cases) => MatchExpression(matchee, cases)
      case e ~ None => e
    }

    def exp3: Parser[Expression] = {
      def postfix: Parser[Expression => Expression] =
        PERIOD ~~ name <^^> (m => (e: Expression) => {
          val res = MemberAccess(e, m)
          res.position := m.position()
          res
        }) |
          LPAREN ~~ repsep(expression, COMMA) ~~ RPAREN <^^> (args => e => {
            val res = FunctionApplication(e, args)
            res.position := e.position()
            res
          })

      exp4 ~~ rep(postfix) <^^> {
        case e ~ postfixes => postfixes.foldLeft(e)(_.pipe(_))
      }
    }

    def exp4: Parser[Expression] =
      name <^^> NamedExpression | // id
        THIS <^^> (_ => ThisExpression()) | // this
        LPAREN ~~ expression ~~ RPAREN | // ( exp ), parenthesised expressions
        LPAREN ~~ expression ~~ COMMA ~~ RPAREN <^^> (e => TupleExpression(e :: Nil)) | // (id ,) for unary tuples
        LPAREN ~~ repsep(expression, COMMA) ~~ RPAREN <^^> TupleExpression | // tuples of arbitrary length
        LCURL ~~ rep(import_definition) ~~ rep(local_definition) ~~ expression ~~ RCURL <^^> CompoundExpression // compound expression with local definitions

    def parameter: Parser[Parameter] = ident ~~ COLON ~~ commit(type_expression) <^^> Parameter

    def upperBound: Grammar.Parser[Option[TypeExpression]] = opt(Tokens.LTCOLON ~~ type_expression)

    def type_parameter: Parser[TypeParameter] = {
      def variance: Parser[Variance] = opt(PLUS ^^^ CoVariance | MINUS ^^^ ContraVariance) <^^> (_.getOrElse(InVariance))

      variance ~~ ident <^^> TypeParameter // TODO
    }

    def type_parameter_list: Parser[List[TypeParameter]] = LBRACK ~~ rep1sep(type_parameter, COMMA) ~~ RBRACK

    def type_parameter_no_variances: Parser[TypeParameter] = (success(InVariance) ~~ ident) <^^> TypeParameter
    def type_parameter_list_no_variances: Parser[List[TypeParameter]] = LBRACK ~~ rep1sep(type_parameter_no_variances, COMMA) ~~ RBRACK

    def match_case: Parser[MatchCase] = CASE ~~ commit(pattern ~~ ARROW ~~ commit(expression)) <^^> MatchCase
    def pattern: Parser[Pattern] =
      TUPLE ~~ commit(LPAREN ~~ repsep(pattern, COMMA) ~~ RPAREN) <^^> TuplePattern |
        LPAREN ~~ commit(expression) ~~ RPAREN <^^> ObjectPattern |
        type_expression ~~ LPAREN ~~ repsep(pattern, COMMA) ~~ RPAREN <^^> ValuePattern |
        ident <^^> NamedPattern |
        UNDERSCORE <^^> (_ => WildcardPattern())

    def module: Grammar.Parser[absyn.Module] = MODULE ~~ rep1sep(ident, PERIOD) ~~ rep(import_definition) ~~ rep(module_level_definition) <^^> absyn.Module
  }

  def apply(tokenReader: Reader[Token[TokenClass]]): absyn.Module = Grammar.phrase(Grammar.module)(tokenReader) match {
    case Grammar.Success(exp, _) => exp
    case Grammar.NoSuccess(msg, rest) => AtPosition(rest.pos) {
      throw TPLError.SyntaxError(msg)
    }
  }

  def parseProject(path: Seq[String]): Program = {
    def collect(f: File): List[File] = {
      if (f.isDirectory) {
        val (files, dirs) = f.listFiles().toList.partition(_.isFile)

        files ::: dirs.flatMap(collect)
      }
      else List(f)
    }

    Program(path.map(new File(_)).flatMap(collect).map(f => Parser(Lexer(Paths.get(f.getPath)))).toList)
  }
}
