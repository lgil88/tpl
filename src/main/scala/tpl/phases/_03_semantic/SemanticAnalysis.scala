package tpl.phases._03_semantic

import tpl.Table.{DefinitionEntry, ModuleEntry, SymbolTable}
import tpl.Types.{AnyType, TplClassId, TplClassType, CoVariance, ContraVariance, FunctionType, GenericType, InVariance, MethodType, NothingType, TupleType, Type, TplTypeVariable}
import tpl.absyn.Definition._
import tpl.absyn.Expression.{MatchCase, QualifiedNamedExpression}
import tpl.absyn._
import tpl.{TPLError, Types, absyn}

import scala.annotation.tailrec
import scala.util.chaining.scalaUtilChainingOps
import scala.util.parsing.input.{NoPosition, Position}

object SemanticAnalysis {
  implicit class TypeUtilities(self: Type) {
    def expect(expected: Type): Unit = if (self.⊈(expected)) throw TPLError.TypeMismatch(expected, self)
  }

  def AtPosition[T](pos: Position)(exp: => T): T = try {
    exp
  } catch {
    case e: TPLError =>
      if (e.pos == NoPosition) e.pos = pos
      throw e
    case e: Throwable => throw e
  }

  case class InContext(context: SymbolTable) {
    private def checkContext(w: WithContext): Unit = {
      // Ensure context checks out if a context is already present
      if (w.context.hasValue && (context ne w.context())) assert(false) // Internal consistency error

      w.context := context // Save the context of this node
    }

    def resolveName(exp: Expression): Expression = {
      def helper(exp: Expression): Option[Module] = {
        exp match {
          case exp@Expression.NamedExpression(name@Name(id, type_parameters)) =>
            // Simple named expressions are ALWAYS qualified names (without a qualifier)

            context.lookup(id, true) match {
              case ModuleEntry(module) =>
                exp.qualified_name := QualifiedName(module.path_with_root, Nil)

                Some(module)
              case DefinitionEntry(d: ModuleLevelDefinition) if d.qualified_name.hasValue =>
                exp.qualified_name := d.qualified_name().copy(type_parameters = type_parameters)

                None
              case _ =>
                exp.qualified_name := QualifiedName(List(id), type_parameters)
                None
            }

          case exp@Expression.MemberAccess(obj, name@Name(id, type_parameters)) =>
            helper(obj) match {
              case Some(value) =>
                exp.qualified_name := QualifiedName(value.path :+ id, type_parameters)
                value.names(id).left.toOption
              case None => None
            }
          case _ => None // resolve is not supported for other expressions
        }
      }

      helper(exp)

      exp // Note to my future self: This is indeed correct. The helper function relies on side effects.
    }

    def analyze(expression: Expression): Type = {
      AtPosition(expression.position()) {

        if (expression.type_info.hasValue) return expression.type_info() // Already typed? instantly return    

        checkContext(expression)

        if (expression.type_info.being_calculated) throw TPLError.CyclicTypeDependency
        expression.type_info.beginCalculation()

        val typ: Type = resolveName(expression) match {
          case Expression.TupleExpression(elements) =>
            elements.foreach(analyze) // The context for elements is the same as for the tuple itself.
            TupleType(elements.map(_.type_info()))
          case Expression.FunctionApplication(callee, arguments) =>
            analyze(callee)
            arguments.foreach(analyze)

            callee.type_info() match {
              case Types.FunctionType(parameterTypes, resultType) =>
                if (parameterTypes.length != arguments.length) throw TPLError.WrongNumberOfArguments()

                arguments zip parameterTypes foreach {
                  case (arg, par) => AtPosition(arg.position()) {
                    arg.type_info().expect(par)
                  }
                }

                resultType
              case _ => throw TPLError.CallOfNonFunction()
            }
          case Expression.LambdaExpression(params, resultType, body) =>
            val childContext = new SymbolTable(context, false)

            params.foreach(childContext.declare) // Enter parameters into the local scope
            params.foreach(InContext(childContext).analyze)

            if (resultType.isDefined) {
              // Lambda is explicitly typed. Set the expression type early.
              resultType.foreach(InContext(childContext).analyze)

              expression.type_info := FunctionType(params.map(_.typ.type_info()), resultType.get.type_info())
            }


            AtPosition(body.position()) {
              val t = InContext(childContext).analyze(body)
              if (resultType.isDefined) t.expect(resultType.get.type_info().asInstanceOf[FunctionType].resultType)
            }

            // Depending on whether the result type is explicitly given, check for type mismatches and determine the type of the lambda
            if (resultType.isDefined) {
              // Type was already set early
              expression.type_info()
            } else {
              // Type is only known after analyzing the body
              FunctionType(params.map(_.typ.type_info()), resultType.map(_.type_info()).getOrElse(body.type_info()))
            }

          case QualifiedNamedExpression(name@QualifiedName(qualifier, type_parameters)) =>

            context.lookup(name, analyse_if_needed = true) match {
              case DefinitionEntry(definition: ClassDefinition) => definition.type_info().instantiate(type_parameters.map(analyze)).asInstanceOf[TplClassType].constructor.get
              case DefinitionEntry(definition: ValueDefinition) => definition.type_info().instantiate(type_parameters.map(analyze))
              case _ => throw TPLError.NotAValue(name)
            }

          case absyn.Expression.MemberAccess(obj, Name(id, type_parameters)) =>
            analyze(obj) /*.bound*/ match {
              case t: TplClassType => // TODO: Type Variables with type bounds
                t.access(id, type_parameters.map(analyze))
              case t => throw TPLError.NotAClassType(t)
            }

          case exp@Expression.CompoundExpression(imports, localDefinitions, result) =>
            exp.scope := new SymbolTable(expression.context(), false)

            localDefinitions.foreach(_.context := exp.scope())
            localDefinitions.foreach(exp.scope().declare) // Declare all definitions of this local context
            imports.foreach(exp.scope().declare) // Resolve all imports

            localDefinitions.foreach(InContext(exp.scope()).analyze) // Analyze the local definitions

            InContext(exp.scope()).analyze(result) // Dive into the local context to analyze the result expression

            result.type_info()
          case Expression.MatchExpression(matchee, cases) =>

            def analyzePattern(given: Type, pattern: Pattern): Unit = {
              if (pattern.type_info.hasValue) return // Already typed? instantly return

              checkContext(pattern)

              if (pattern.type_info.being_calculated) throw TPLError.CyclicTypeDependency
              pattern.type_info.beginCalculation()

              pattern.type_info := `given`

              pattern match {
                case Pattern.TuplePattern(patterns) =>
                  patterns.foreach(_.context := pattern.context())

                  `given` match {
                    case AnyType =>
                      patterns.foreach(analyzePattern(AnyType, _))

                    case TupleType(parameterTypes) =>
                      if (parameterTypes.length != patterns.length) throw TPLError.WrongNumberOfArguments()
                      parameterTypes.zip(patterns).foreach(Function.tupled(analyzePattern))
                    case _ => throw TPLError.TypeMismatch(`given`, AnyType)
                  }
                case Pattern.ObjectPattern(expression) =>
                  expression.context := pattern.context()

                  resolveName(expression) match {
                    case QualifiedNamedExpression(name) =>
                      expression.context().lookup(name, analyse_if_needed = true) match {
                        case DefinitionEntry(definition: ValueDefinition) =>

                          if (!(definition.type_info() ⊆ `given`)) throw TPLError.TypeMismatch(`given`, expression.type_info())
                        case _ => throw TPLError.ObjectPatternDoesNotNameObject(name)
                      }
                    case _ => throw TPLError.InvalidExpressionInEqualityPattern
                  }
                case Pattern.ValuePattern(name, argumentPatterns) =>
                  argumentPatterns.foreach(_.context := pattern.context())

                  analyze(name)

                  name.type_info() match {
                    case t: TplClassType =>
                      t.constructorTypes match {
                        case Some(parameterTypes) =>
                          if (parameterTypes.length != argumentPatterns.length) throw TPLError.WrongNumberOfArguments()
                          parameterTypes.zip(argumentPatterns).foreach(Function.tupled(analyzePattern))
                      }
                  }

                  if (!(name.type_info() ⊆ `given`)) throw TPLError.TypeMismatch(`given`, name.type_info())

                case Pattern.NamedPattern(_) =>
                case Pattern.WildcardPattern() =>
              }

              pattern.type_info()
            }

            def analyzeCase(given: Type)(cas: MatchCase): Unit = {
              analyzePattern(given, cas.pattern)

              // Create a child context for the case's body
              cas.scope := new SymbolTable(context, false)

              // Declare variables introduced by the pattern
              cas.pattern.freeVariables.foreach(cas.scope().declare)

              // Analyze the body in its context
              InContext(cas.scope()).analyze(cas.body)
            }

            analyze(matchee)

            // Analyze the cases top down with the 
            cases.foreach(analyzeCase(matchee.type_info()))

            cases.map(_.body.type_info()).foldLeft[Type](NothingType)(_ ∪ _)

          case Expression.TypedExpression(base, typ) =>
            analyze(typ)
            analyze(base)

            if (!(base.type_info() ⊆ typ.type_info())) throw TPLError.TypeMismatch(base.type_info(), typ.type_info())

            typ.type_info()
          case Expression.ThisExpression() =>
            context.resolvedThis match {
              case Some(value) => value
              case None => throw TPLError.UndefinedName("this")
            }
        }

        expression.type_info.set(typ)
        expression.type_info()
      }
    }

    def analyze(typeExpression: TypeExpression): Type = {
      AtPosition(typeExpression.position()) {
        if (typeExpression.type_info.hasValue) return typeExpression.type_info() // Already typed? instantly return
        if (!typeExpression.expected_variance.hasValue) typeExpression.expected_variance := InVariance

        val variance = typeExpression.expected_variance()

        checkContext(typeExpression)

        if (typeExpression.type_info.being_calculated) throw TPLError.CyclicTypeDependency
        typeExpression.type_info.beginCalculation()

        val typ: Type = typeExpression match {
          case TypeExpression.NamedTypeExpression(name) =>
            context.lookup(name, analyse_if_needed = true) match {
              case DefinitionEntry(definition) =>
                definition match {
                  case defi: TypeDefinition =>

                    // Check variancs
                    defi.type_info() match {
                      case GenericType(parameters, _) =>
                        if (parameters.isEmpty && name.type_parameters.nonEmpty) throw TPLError(s"Type ${name.qualifier.mkString(".")} does not accept parameters")
                        if (parameters.nonEmpty && name.type_parameters.isEmpty) throw TPLError(s"Type ${name.qualifier.mkString(".")} needs type parameters")
                        if (parameters.size != name.type_parameters.size) throw TPLError(s"Parameter count mismatch for type ${name.qualifier.mkString(".")}")

                        parameters.map(_.typeParameter.variance).zip(name.type_parameters) foreach {
                          case (CoVariance, texp) => texp.expected_variance := variance
                          case (ContraVariance, texp) => texp.expected_variance := variance.opposite
                          case (InVariance, texp) => texp.expected_variance := InVariance
                        }

                      case TplTypeVariable(typeParameter /*, upperBound*/) =>
                        if (!typeParameter.variance.compatible(typeExpression.expected_variance()))
                          throw TPLError.VarianceMismatch(variance, typeParameter.variance, typeParameter.name)

                      case _ =>
                    }

                    defi.type_info().instantiate(name.type_parameters.map(analyze))
                  case _ => throw TPLError.NotAType(name)
                }
            }
          case TypeExpression.FunctionTypeExpression(params, result) =>
            params.foreach(_.expected_variance := variance.opposite)
            result.expected_variance := variance
            FunctionType(params.map(analyze), analyze(result))
          case TypeExpression.TupleTypeExpression(parts) =>
            parts.foreach(_.expected_variance := variance)
            TupleType(parts.map(analyze))
          case TypeExpression.AnyTypeExpression() => AnyType
          case TypeExpression.NothingTypeExpression() => NothingType
        }

        typeExpression.type_info.set(typ)

        typeExpression.type_info()
      }
    }

    def analyze(definition: Definition): Type = {
      AtPosition(definition.position()) {

        if (definition.type_info.hasValue) return definition.type_info() // Already typed? instantly return

        checkContext(definition)

        if (definition.type_info.being_calculated) throw TPLError.CyclicTypeDependency
        definition.type_info.beginCalculation()

        val typ: Type = definition match {
          case Definition.ValDefinition(_, Some(typ), value) =>
            InContext(context).analyze(typ)
            definition.type_info := typ.type_info()

            InContext(context).analyze(value)
            if (!(value.type_info() ⊆ typ.type_info())) throw TPLError.TypeMismatch(typ.type_info(), value.type_info())
            typ.type_info()

          case Definition.ValDefinition(_, None, value) =>
            analyze(value)
            value.type_info()

          case tp: TypeParameter =>
            // tp.upperBound.foreach(analyze)

            val tv = TplTypeVariable(tp /*, tp.upperBound.map(_.type_info()).getOrElse(AnyType)*/)

            tv.origin := tp

            tv
          case Definition.FunctionDeclaration(_, typeParameters, parameters, resultType) =>
            val childContext = new SymbolTable(context, false)

            typeParameters.foreach(childContext.declare) // Enter type parameters into the local scope          
            typeParameters.foreach(analyze) // Analyze type parameters to create type variables

            parameters.foreach(childContext.declare) // Enter parameters into the local scope
            parameters.foreach(InContext(childContext).analyze)

            InContext(childContext).analyze(resultType)

            val t1 = FunctionType(parameters.map(_.type_info()), resultType.type_info())
            definition.type_info := (if (typeParameters.nonEmpty) GenericType(typeParameters.map(_.type_info().asInstanceOf[TplTypeVariable]), t1) else t1)

            definition.type_info()

          case definition@Definition.ClassDefinition(_, typeParameters, constructorParameters, extension, members) =>

            // Set Variances.
            constructorParameters.foreach(_.typ.expected_variance := CoVariance)
            extension.foreach(_.expected_variance := CoVariance)
            members.foreach({
              case Definition.FunctionDefinition(_, typeParameters, parameters, resultType, _) =>
                parameters.foreach(_.typ.expected_variance := ContraVariance)
                //typeParameters.foreach(_.upperBound.foreach(_.expected_variance := ContraVariance))
                resultType.expected_variance := CoVariance
            })

            val childContext = new SymbolTable(context, false)
            members.foreach(_.context := childContext)

            typeParameters.foreach(childContext.declare) // Enter type parameters into the local scope          
            typeParameters.foreach(analyze) // Analyze type parameters to create type variables

            // Note: Parameters and members are NOT entered into the scope, they are only available via explicit access with this.<name>

            // Analyze the optional extension
            extension.map(InContext(childContext).analyze).foreach({
              case t: TplClassType => if (!t.isAbstract) throw TPLError.IllegalInheritanceNonAbstractClass(t)
              case t => throw TPLError.NotAClassType(t)
            })

            // Create an id for this class and the class type
            val id = TplClassId(definition, typeParameters.map(_.variance), typeParameters.map(_.type_info()))
            val t1 = TplClassType(id, extension.map(_.type_info().asInstanceOf[TplClassType]), Some(constructorParameters), members.map(m => m.name -> m).toMap, None, false)

            // Now the type is known
            definition.type_info := (if (typeParameters.nonEmpty) GenericType(typeParameters.map(_.type_info().asInstanceOf[TplTypeVariable]), t1) else t1)
            childContext.thisType := t1

            // As the type is now known we can analyze the constructor parameters. This is needed because they can reference the class in recursive data structures
            constructorParameters.foreach(InContext(childContext).analyze)

            // We can also analyze the other members
            members.foreach(InContext(childContext).analyze)

            definition.type_info()

          case definition@Definition.ObjectDefinition(_, extension, members) =>
            val childContext = new SymbolTable(context, false)
            members.foreach(_.context := childContext)

            // Analyze the optional extension
            extension.map(InContext(childContext).analyze).foreach({
              case t: TplClassType => if (!t.isAbstract) throw TPLError.IllegalInheritanceNonAbstractClass(t)
              case t => throw TPLError.NotAClassType(t)
            })

            // Create an id for this object's class type
            val id = TplClassId(definition, Nil, Nil)
            val t = TplClassType(id, extension.map(_.type_info().asInstanceOf[TplClassType]), None, members.map(m => m.name -> m).toMap, None, false)

            // Now the type is known
            definition.type_info := t
            childContext.thisType := definition.type_info()

            // We can also analyze the other members
            members.foreach(InContext(childContext).analyze)

            definition.type_info()

          case definition@Definition.TraitDefinition(_, typeParameters, extension, members) =>
            extension.foreach(_.expected_variance := CoVariance)
            members.foreach({
              case Definition.FunctionDeclaration(_, typeParameters, parameters, resultType) =>
                parameters.foreach(_.typ.expected_variance := ContraVariance)
                //typeParameters.foreach(_.upperBound.foreach(_.expected_variance := ContraVariance))

                resultType.expected_variance := CoVariance

              case Definition.FunctionDefinition(_, typeParameters, parameters, resultType, _) =>
                parameters.foreach(_.typ.expected_variance := ContraVariance)
                //typeParameters.foreach(_.upperBound.foreach(_.expected_variance := ContraVariance))
                resultType.expected_variance := CoVariance
            })

            val childContext = new SymbolTable(context, false)
            members.foreach(_.context := childContext)

            typeParameters.foreach(childContext.declare) // Enter type parameters into the local scope          
            typeParameters.foreach(analyze) // Analyze type parameters to create type variables

            // Note: Parameters and members are NOT entered into the scope, they are only available via explicit access with this.<name>

            // Analyze the optional extension
            extension.map(InContext(childContext).analyze).foreach({
              case t: TplClassType => if (!t.isAbstract) throw TPLError.IllegalInheritanceNonAbstractClass(t)
              case t => throw TPLError.NotAClassType(t)
            })

            // Create an id for this class and the class type
            val id = TplClassId(definition, typeParameters.map(_.variance), typeParameters.map(_.type_info()))
            val t1 = TplClassType(id, extension.map(_.type_info().asInstanceOf[TplClassType]), None, members.map(m => m.name -> m).toMap, None, true)

            // Now the type is known
            definition.type_info := (if (typeParameters.nonEmpty) GenericType(typeParameters.map(_.type_info().asInstanceOf[TplTypeVariable]), t1) else t1)
            childContext.thisType := t1

            // We can also analyze the other members
            members.foreach(InContext(childContext).analyze)

            definition.type_info()
          case Definition.FunctionDefinition(_, typeParameters, parameters, resultType, body) =>
            val childContext = new SymbolTable(context, false)

            typeParameters.foreach(childContext.declare) // Enter type parameters into the local scope          
            typeParameters.foreach(analyze) // Analyze type parameters to create type variables

            parameters.foreach(childContext.declare) // Enter parameters into the local scope
            parameters.foreach(InContext(childContext).analyze)

            InContext(childContext).analyze(resultType)

            val t1 = FunctionType(parameters.map(_.type_info()), resultType.type_info())
            definition.type_info := (if (typeParameters.nonEmpty) GenericType(typeParameters.map(_.type_info().asInstanceOf[TplTypeVariable]), t1) else t1)

            AtPosition(body.position()) {
              InContext(childContext).analyze(body).expect(t1.resultType)
            }

            definition.type_info()

          case Definition.Parameter(_, typ) =>
            analyze(typ)

            typ.type_info()
        }

        definition.type_info.set(typ)
        definition.type_info()
      }
    }

    def analyze(obj: WithContext): Unit = {
      obj match {
        case e: Expression => analyze(e)
        case d: Definition => analyze(d)
        case t: TypeExpression => analyze(t)
      }
    }
  }

  def analyze(obj: WithContext): Unit = {
    InContext(obj.context()).analyze(obj)
  }

  def apply(program: Program): Unit = {

    // Create module hierarchy
    {
      /**
       * Reduces a list of modules to their root modules.
       * Creates virtual modules in cases where the parent module does not exist.
       * If A.B.C and A.B.D exist but A.B does not exist explicitly it is created as a virtual module with no definitions beside its submodules.
       * In the process is also annotated the list of submodules of each module into the AST.
       *
       * @param modules The list of modules to reduce.
       * @return A List of root modules with no parents
       */
      def reduceModuleTree(modules: List[absyn.Module]): absyn.Module = {
        @tailrec
        def helper(modules: List[absyn.Module]): absyn.Module = {
          modules match {
            case head :: Nil if head.path.isEmpty =>
              // There is only the virtual root module left. Return it
              head
            case head :: tail =>
              // Find all 
              val (siblings, nonSiblings) = tail.partition(_.path.init == head.path.init)

              tail.find(_.path == head.path.init) match {
                case Some(parent) =>
                  parent.submodules.set(head :: siblings)
                  helper(nonSiblings) // Continue reduction of all nonSiblings
                case None =>
                  // The parent module does not exist explicitly. Create a new virtual module that just serves as a container for the submodules
                  val parent = absyn.Module(head.path.init, Nil, Nil)
                  parent.submodules.set(head :: siblings)

                  // Add the virtual parent along with any nonSiblings to the remaining worklist
                  helper(nonSiblings :+ parent)
              }
          }
        }

        helper(modules.sortBy(-_.path.length))
      }

      program.root_module := reduceModuleTree(program.modules)

      // Set submodules for each leaf module to be empty
      program.modules.foreach(m => if (!m.submodules.hasValue) m.submodules := Nil)
    }

    // Create a hierarchical symbol table for all modules
    {
      def build(parent: SymbolTable, isRoot: Boolean)(module: Module): Unit = {
        module.context := parent

        module.scope := new SymbolTable(parent, true)

        // Declare submodules and definitions so they can see each other (siblings are visible)
        module.submodules().foreach(s => module.scope().declare(s))
        module.definitions.foreach(module.scope().declare)
        module.definitions.foreach(d => d.qualified_name := QualifiedName(module.path_with_root :+ d.name, Nil))

        module.definitions.foreach(_.context.set(module.scope()))

        if (isRoot) module.submodules().foreach(build(module.scope(), isRoot = false))
        else module.submodules().foreach(build(parent, isRoot = false))
      }

      build(new SymbolTable(program.root_module()), isRoot = true)(program.root_module())
    }

    // Create a hierarchical symbol table for all modules
    {
      def resolveImports(module: Module): Unit = {
        // Declare submodules and definitions so they can see each other (siblings are visible)

        module.imports.foreach(module.scope().declare)
        module.submodules().foreach(resolveImports)
      }

      resolveImports(program.root_module())
    }

    // Analyze all definitions at module scope. Topological sorting is implicitly done on cross references
    {
      def collectDefinitions(module: Module): List[Definition] = module.definitions ::: module.submodules().flatMap(collectDefinitions)

      collectDefinitions(program.root_module()).foreach(analyze)
    }

    // Check all classes for consistency regarding inheritance and overrides.
    // This has to be done in a second pass to avoid false cyclical dependencies.
    {
      def checkMethodConsistency(classType: TplClassType): Unit = {
        classType.methodTypes.foreach({
          case (n, MethodType(t, _, d)) => AtPosition(d.position())(classType.baseClass.foreach(_.allMethodTypes.get(n).map(_.t).foreach(bt => if (t ⊈ bt) throw TPLError.IllegalOverride(n, bt, t))))
        })

        if (!classType.isAbstract && classType.abstractMethods.nonEmpty) {
          def t(v: (String, MethodType)): String = v pipe { case (n, mt) => s"'$n' [${mt.t}]" }

          if (classType.abstractMethods.size == 1) throw TPLError(s"Class $classType does not override abstract method ${t(classType.abstractMethods.head)}")
          else throw TPLError(s"Class $classType does not override abstract methods ${classType.abstractMethods.map(t).mkString(", ")}")
        }
      }

      val classTypes = program.modules.flatMap(_.definitions).filter(_.definesClass).map(_.type_info().underlying).map(_.asInstanceOf[TplClassType])

      classTypes.foreach(checkMethodConsistency)
    }
  }
}
