package tpl.phases._05_codegen

import tpl.phases._05_codegen.CppAbsyn.AST

object Constants {

  import CppAbsyn.ConstructionHelpers._

  val SYSTEM_NAME_PREFIX = "_tpl_"
  val ROOT_MODULE_NAME: String = SYSTEM_NAME_PREFIX + "root_module"
  val WILDCARD_PATTERN_NAME = SYSTEM_NAME_PREFIX + "wildcard"
  val DUMMY_NAME = SYSTEM_NAME_PREFIX + "dummy"
  val MATCHEE_NAME = SYSTEM_NAME_PREFIX + "matchee"
  val CLASS_TYPE_NAME = SYSTEM_NAME_PREFIX + "instance"
  val VTABLE_NAME = SYSTEM_NAME_PREFIX + "vtable"
  val SELF_NAME = SYSTEM_NAME_PREFIX + "self"
  val LOCAL_SCOPE_RESULT_NAME = SYSTEM_NAME_PREFIX + "scope_result"
  val EXPRESSION_VALUE = SYSTEM_NAME_PREFIX + "value"
  val FUNCTION_APPLY_RESULT = SYSTEM_NAME_PREFIX + "result"
  val ANONYMOUS_FUNCTION = SYSTEM_NAME_PREFIX + "anon_func"
  val MATCH_VALUE = SYSTEM_NAME_PREFIX + "match_value"
  val LOCAL_SCOPE_DEFINITION = SYSTEM_NAME_PREFIX + "local_scope"
  val SINGLETON_INSTANCE = SYSTEM_NAME_PREFIX + "singleton_instance"
  val CLASS_VALUE = SYSTEM_NAME_PREFIX + "class"
  val BASE_CLASS_NAME = SYSTEM_NAME_PREFIX + "super"
  val CONSTRUCTOR_NAME = SYSTEM_NAME_PREFIX + "constructor"
  val PATTERN_MATCH_DEDUCTION_HACK_PREFIX = SYSTEM_NAME_PREFIX + "deduction_hack"

  val APPLY_NAME = SYSTEM_NAME_PREFIX + "apply"

  val TPL_RUNTIME_NAMESPACE = AST.Name("_tpl_runtime")
  val TPL_RUNTIME_TUPLE = TPL_RUNTIME_NAMESPACE --> "tuple"
  val TPL_RUNTIME_FIX = TPL_RUNTIME_NAMESPACE --> "fix"
  val TPL_RUNTIME_UNDEFINED = TPL_RUNTIME_NAMESPACE --> "undefined"
  val TPL_RUNTIME_FIX_RESULT = AST.Name("fixed")
  val TPL_MODULE_PREFIX = SYSTEM_NAME_PREFIX + "module_"

  val MATCHER_NAME = SYSTEM_NAME_PREFIX + "matcher"
  def matcher(i: Int) = MATCHER_NAME + i

  object ChildExpressions {
    val sig = SYSTEM_NAME_PREFIX + "cxp_"
    val BODY = sig + "body"
    val MATCHEE = sig + "matchee"
    val CALLEE = sig + "callee"
    val OBJECT = sig + "obj"
    val LAST = sig + "last"
    val TUPLE_ELEMENT = sig + "el"
    val ARGUMENT = sig + "arg"

    def tuple_element(i: Int): String = TUPLE_ELEMENT + i
    def argument(i: Int): String = ARGUMENT + i
    def variable(name: String) = SYSTEM_NAME_PREFIX + "varexp_" + name
  }

  val APPLY_IMPLEMENTATION = SYSTEM_NAME_PREFIX + "apply_impl"

  val FIXED_ROOT_MODULE = "root"

  def scope_parameter_name(n: Int) = SYSTEM_NAME_PREFIX + "scope" + n
  val TOSTRING_NAME = SYSTEM_NAME_PREFIX + "str"

  def fix(n: AST.Name): AST.Name = TPL_RUNTIME_FIX.<>(n :: Nil) --> TPL_RUNTIME_FIX_RESULT

  val reservedNames = List(FIXED_ROOT_MODULE, TOSTRING_NAME, APPLY_NAME, TPL_RUNTIME_NAMESPACE)
}
