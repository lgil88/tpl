package tpl.phases._05_codegen

import tpl.Types.{GenericType, TplClassType}
import tpl.absyn.Definition.ModuleLevelDefinition
import tpl.absyn.Expression._
import tpl.absyn.Pattern._
import tpl.absyn.TypeExpression.NamedTypeExpression
import tpl.absyn._
import tpl.phases._04_varalloc
import tpl.phases._04_varalloc.{ConstructorLocation, Location, SingletonObjectLocation}
import tpl.phases._05_codegen.CppAbsyn.AST
import tpl.phases._05_codegen.CppAbsyn.AST.{Definition => _, Program => _, _}
import tpl.{CommandLineOptions, Table, Types, absyn}

import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Paths}
import scala.io.Source

class CodeGen(options: CommandLineOptions) {

  import CppAbsyn.ConcreteSyntaxHelpers.RichProgram
  import CppAbsyn.ConstructionHelpers._
  import tpl.phases._05_codegen.CppAbsyn.{AST => cpp}

  private implicit class RichModule(m: absyn.Module) {
    def structName: String = m.path.mkString(Constants.TPL_MODULE_PREFIX, "_", "")

    def fileName: String = s"$structName.hpp"
  }

  def apply(program: Program): Unit = {
    Files.createDirectories(Paths.get(this.options.outDirectory))

    Files.write(Paths.get(s"${options.outDirectory}/tpl_runtime.hpp"), Source.fromResource("tpl_runtime.hpp").mkString.getBytes(StandardCharsets.UTF_8))

    apply(program.root_module())
  }

  def apply(module: absyn.Module): Unit = {
    val includes = IncludeLocal("tpl_runtime.hpp") :: module.submodules().map(m => IncludeLocal(m.fileName))

    val submoduleDeclarations = module.submodules().map(m => UsingDefinition(m.path.drop(module.path.size).head, CppAbsyn.AST.SimpleName(options.namespace).-->(m.structName).<>(Constants.ROOT_MODULE_NAME)))
    val moduleLevelDefinitions = module.definitions.map(generate).flatMap(d => d.log.definitions :+ d.value)

    val moduleStruct = cpp.StructDefinition(module.structName, submoduleDeclarations ::: moduleLevelDefinitions).templated(cpp.TemplateParameter(cpp.TypenameType, Constants.ROOT_MODULE_NAME) :: Nil)

    val programDefinitions =
      NamespaceDefinition(options.namespace, moduleStruct :: Option.when(module.path.isEmpty)(UsingDefinition(Constants.FIXED_ROOT_MODULE, Constants.fix(AST.Name(module.structName)))).toList)

    val p = CppAbsyn.AST.Program(PragmaOnce :: includes,
      programDefinitions :: Nil)

    Files.write(Paths.get(s"${options.outDirectory}/${module.fileName}"), p.toCpp.getBytes(StandardCharsets.UTF_8))

    module.submodules().foreach(apply)
  }

  def constructFunction(name: String, body: CodeGenWriter[AST.Name], parameterNames: List[String], tosName: String): CodeGenWriter[AST.Name] = {
    val pars = if (parameterNames.isEmpty) List(Constants.DUMMY_NAME) else parameterNames

    body.lambdaImplementation(Constants.APPLY_IMPLEMENTATION, pars).lambda(name, pars, tosName)
  }

  def constructFunctionCall(function: CodeGenWriter[cpp.Name], arguments: CodeGenWriter[List[cpp.Name]]): CodeGenWriter[cpp.Name] = {
    val args = if (arguments.value.isEmpty) pure(List(Constants.TPL_RUNTIME_UNDEFINED)) else arguments

    for (callee_exp <- function; arg_exps <- args) yield callee_exp --> Constants.APPLY_NAME.<>(arg_exps)
  }

  class WrappedClassType(name: String, self: TplClassType) {
    private lazy val parameterNames = self.constructorParams.toList.flatMap(_.map(_.name)).map(AST.Name.apply)
    private lazy val prefixedParameterNames = parameterNames.map("_" + _.name).map(cpp.Name.apply)

    private lazy val v_table: StructDefinition = {

      val curried_members = self.methodSources.map({
        case (method_name, src) =>
          src match {
            case Types.DefinitionSource(_: absyn.Definition.FunctionDeclaration) =>
              UsingDefinition(method_name, Constants.TPL_RUNTIME_UNDEFINED)
            case Types.DefinitionSource(dec: absyn.Definition.FunctionDefinition) =>
              val generated_members = generate(dec).value

              val n = generated_members match {
                case m: UsingDefinition => m.name
                case m: StructDefinition => m.name
                case m: ClassDefinition => m.name
              }

              val curried = constructFunction(n.baseName, CodeGenWriter(n, WriterTarget(generated_members :: Nil)), Constants.SELF_NAME :: Nil, s"method $name.$n")

              curried.log.definitions.head // UsingDefinition(method_name, curried)
            case Types.InheritanceSource(cls) =>
              UsingDefinition(method_name, generate(self.id.identity.context().lookup(cls.id.identity.qualified_name(), analyse_if_needed = true).asInstanceOf[Table.DefinitionEntry].definition.location()).-->(Constants.VTABLE_NAME).-->(method_name))
          }
      })

      StructDefinition(Constants.VTABLE_NAME, curried_members.toList)
    }

    private lazy val constructor: AST.Definition = {
      val constructor_apply: AST.Definition = {
        val par_names = if (parameterNames.isEmpty) List(AST.Name(Constants.DUMMY_NAME)) else parameterNames

        cpp.UsingDefinition(Constants.APPLY_NAME, Constants.CLASS_TYPE_NAME.<>(par_names)).templated(par_names.map(n => cpp.TemplateParameter(cpp.TypenameType, n.name)))
      }

      val constructorToString: FunctionDefinition = FunctionDefinition(Constants.TOSTRING_NAME, StringLiteral(s"<constructor $name>"))

      StructDefinition(Constants.CONSTRUCTOR_NAME, List(constructor_apply, constructorToString))
    }

    private lazy val baseClassDefinition = UsingDefinition(Constants.BASE_CLASS_NAME, generate(self.baseClass.get.id.identity.location()))

    private lazy val internalType: AST.Definition = {
      val toStringMethod: FunctionDefinition = {
        val body =
          if (self.constructor.isDefined)
            StringConcatenation(StringLiteral(s"$name(") :: (parameterNames.map(a => FunctionCall(a --> Constants.TOSTRING_NAME)).flatMap(a => List(StringLiteral(", "), a)).drop(1) :+ StringLiteral(")")))
          else
            StringConcatenation(StringLiteral(name) :: Nil)

        FunctionDefinition(Constants.TOSTRING_NAME, body)
      }

      val base = StructDefinition(Constants.CLASS_TYPE_NAME, (UsingDefinition(Constants.CLASS_VALUE, name) :: (parameterNames zip prefixedParameterNames).map(Function.tupled(cpp.UsingDefinition))) :+ toStringMethod)

      if (self.constructor.isDefined) {
        val template_param_names = if (prefixedParameterNames.isEmpty) List(AST.Name(Constants.DUMMY_NAME)) else prefixedParameterNames

        base.templated(template_param_names.map(n => cpp.TemplateParameter(cpp.TypenameType, n.name)))
      } else base
    }

    lazy val full: StructDefinition = cpp.StructDefinition(name, List(
      Option.when(self.baseClass.isDefined)(baseClassDefinition),
      Some(v_table),
      Option.when(!self.isAbstract)(internalType),
      Option.when(self.isSingletonObject)(UsingDefinition(Constants.SINGLETON_INSTANCE, Constants.CLASS_TYPE_NAME)),
      Option.when(self.constructor.isDefined)(constructor)
    ).flatten)
  }

  def constructClass(name: String, `type`: TplClassType): CodeGenWriter[StructDefinition] = pure(new WrappedClassType(name, `type`).full)

  implicit class LocationSyntax(self: Definition) {
    def valueLocation: Location = self match {
      case definition: ModuleLevelDefinition if definition.definesClass =>
        val typ = definition.type_info() match {
          case Types.GenericType(_, underlying: TplClassType) => underlying
          case t: TplClassType => t
        }

        if (typ.isSingletonObject) SingletonObjectLocation(definition.location())
        else if (typ.isRegularClass) ConstructorLocation(definition.location())
        else ???

      case _ => self.location()
    }
  }

  def generate(location: Location): AST.Name = location match {
    case _04_varalloc.SimpleNamedLocation(name) => name
    case _04_varalloc.ScopeLocation(scope, name) => SimpleName(scope.scopeName) --> Constants.TPL_RUNTIME_FIX_RESULT --> name
    case _04_varalloc.ModuleLocation(module, name) =>
      val fixed_root = SimpleName(Constants.ROOT_MODULE_NAME) --> Constants.TPL_RUNTIME_FIX_RESULT
      val module_name = module.path.foldLeft(fixed_root)(_ --> _)
      module_name --> name
    case _04_varalloc.VariableLocation(base) => generate(base)
    case _04_varalloc.SingletonObjectLocation(base) => generate(base).-->(Constants.SINGLETON_INSTANCE)
    case _04_varalloc.ConstructorLocation(base) => generate(base).-->(Constants.CONSTRUCTOR_NAME)
    case _04_varalloc.NoLocation => ???
  }

  def generate(members: List[Definition]): List[cpp.Definition] =
    members.flatMap(definition => {
      val d = generate(definition)
      Some(d.log.definitions :+ d.value)
    }).flatten

  def generate(definition: absyn.Definition): CodeGenWriter[cpp.Definition] =
    definition match {
      case Definition.ValDefinition(name, _, value) =>
        //generate(value).expression(name, dont_optimize = true).liftDefinitions((_, b) => b.head)

        generate(value).expression(Constants.ChildExpressions.variable(name)).map(CppAbsyn.AST.UsingDefinition(name, _))
      case definition: ModuleLevelDefinition if definition.definesClass =>
        val t = definition.type_info() match {
          case t: Types.GenericType => t.underlying.asInstanceOf[TplClassType]
          case t: TplClassType => t
        }
        constructClass(definition.name, t)
      case Definition.FunctionDefinition(name, _, parameters, _, body) =>
        constructFunction(name, generate(body).expression(Constants.ChildExpressions.BODY), parameters.map(_.name), s"function $name").liftDefinitions((_, a) => a.head)
      case _ => pure(CppAbsyn.AST.UsingDefinition(definition.name, Constants.TPL_RUNTIME_UNDEFINED))
    }

  object IC {
    trait Def {
      protected var refCounter = 0

      protected def createRef(): Unit = refCounter += 1
    }

    case class Raw(raw: cpp.Definition) extends Def {
    }

    case class ExpressionClass(helpers: Seq[IC.Def], value: IC.Name)(name: String) extends Def {
      def ref: IC.Name = {
        this.createRef()
        IC.Name.ExpressionReference(this)
      }

      def canBeInlined: Boolean = this.helpers.isEmpty && refCounter < 2
    }

    case class Metafunction(name: String, parameters: Seq[String], helpers: Seq[IC.Def], result_name: String, value: IC.Name) extends Def {
      def application(args: Seq[IC.Name]): IC.Name.MetafunctionApplication = {
        this.createRef()
        IC.Name.MetafunctionApplication(this, args)
      }

      def fixed(): IC.Name.Fixing = {
        assert(this.parameters.size == 1)

        this.createRef()
        IC.Name.Fixing(this)
      }
    }

    case class MetafunctionClass(name: String, function: Metafunction) extends Def {
      def ref: IC.Name = {
        this.createRef()
        IC.Name.Reference(this)
      }
    }

    sealed trait Name
    object Name {
      sealed trait BasicName extends Name

      case class Simple(name: String) extends BasicName
      case class Raw(name: cpp.Name) extends Name
      case class Reference(defi: IC.Def) extends Name
      case class ExpressionReference(ref: ExpressionClass) extends Name
      case class MetafunctionApplication(ref: Metafunction, args: Seq[Name]) extends Name
      case class MetafunctionClassApplication(base: Name, args: Seq[Name]) extends Name
      case class TemplateInstantiation(base: Name, args: Seq[Name]) extends Name
      case class Fixing(metafun: Metafunction) extends Name
      case class Qualified(base: Name, member: String) extends Name
    }
  }

  def new_generate(defi: absyn.Definition): IC.Def = ???

  def new_generate(exp: absyn.Expression): String => IC.ExpressionClass = exp match {
    case TypedExpression(base, _) =>
      val b = new_generate(base)("base")

      IC.ExpressionClass(Seq(b), b.ref)
    case TupleExpression(elements) =>
      val els = elements.zipWithIndex.map({
        case (e, index) => new_generate(e)(Constants.ChildExpressions.tuple_element(index))
      })

      val typ = IC.Name.TemplateInstantiation(IC.Name.Raw(Constants.TPL_RUNTIME_TUPLE), els.map(_.ref))

      IC.ExpressionClass(els, typ)
    case ThisExpression() =>
      IC.ExpressionClass(Seq(), IC.Name.Simple(Constants.SELF_NAME))
    case FunctionApplication(callee, arguments) =>
      val call = new_generate(callee)(Constants.ChildExpressions.CALLEE)

      val args = arguments.zipWithIndex.map({
        case (element, i) => new_generate(element)(Constants.ChildExpressions.argument(i))
      })

      // Function values are always metafunction classes
      IC.ExpressionClass(call +: args, IC.Name.MetafunctionClassApplication(call.ref, args.map(_.ref)))
    case LambdaExpression(params, _, body) =>
      val bdy = new_generate(body)(Constants.ChildExpressions.BODY)
      val metafun = IC.Metafunction(Constants.APPLY_IMPLEMENTATION, params.map(_.name), Seq(bdy), Constants.MATCH_VALUE, bdy.ref)
      val metafunclass = IC.MetafunctionClass(Constants.ANONYMOUS_FUNCTION, metafun)

      IC.ExpressionClass(Seq(metafunclass), metafunclass.ref)
    case exp@CompoundExpression(_, localDefinitions, result) =>
      val res = new_generate(result)(Constants.ChildExpressions.LAST)
      val locals = localDefinitions.map(new_generate)

      val metafun = IC.Metafunction(Constants.LOCAL_SCOPE_DEFINITION, Seq(exp.scope().scopeName), locals :+ res, Constants.LOCAL_SCOPE_RESULT_NAME, res.ref)

      IC.ExpressionClass(Seq(metafun), IC.Name.Qualified(metafun.fixed(), Constants.LOCAL_SCOPE_RESULT_NAME))

    case _ => IC.ExpressionClass(Seq(), IC.Name.Raw(cpp.SimpleName("void")))
    case QualifiedNamedExpression(qualifiedName) => ???
    case NamedExpression(name) => ???
    case MemberAccess(obj, name) => ???
    case MatchExpression(matchee, cases) => ???
  }

  def generate(exp: absyn.Expression): CodeGenWriter[cpp.Name] = exp match {
    case Expression.TypedExpression(base, _) => generate(base) // Types have no effect during runtime as they are statically checked completely.
    case Expression.TupleExpression(elements) =>
      val els = elements.zipWithIndex.map({
        case (element, i) => generate(element).expression(Constants.ChildExpressions.tuple_element(i))
      }).foldLeft[CodeGenWriter[List[cpp.Name]]](CodeGenWriter.pure(Nil))((a, b) => b ~: a).map(_.reverse)

      els.map(Constants.TPL_RUNTIME_TUPLE.<>(_, force = true))
    case Expression.ThisExpression() => pure(cpp.SimpleName(Constants.SELF_NAME))
    case QualifiedNamedExpression(qualifiedName) =>
      pure(generate(exp.context().lookup(qualifiedName, analyse_if_needed = true).asInstanceOf[Table.DefinitionEntry].definition.valueLocation))
    case Expression.NamedExpression(name) => pure(cpp.SimpleName(name.identifier))
    case Expression.MemberAccess(obj, name) =>
      val isConstructorParam = (obj.type_info() match {
        case t: Types.GenericType => t.underlying.asInstanceOf[TplClassType]
        case t: TplClassType => t
      }).constructorParams.exists(_.exists(_.name == name.identifier))

      val ob = {
        val tmp = generate(obj)

        tmp.expression(Constants.ChildExpressions.OBJECT, !isConstructorParam && tmp.value.hasAnyTemplateInstanciation)
      }

      if (isConstructorParam) ob.map(n => n --> name.identifier)
      else constructFunctionCall(ob.map(n => n --> Constants.CLASS_VALUE --> Constants.VTABLE_NAME --> name.identifier), ob.liftDefinitions((a, _) => a).map(o => List(o)))
    case Expression.FunctionApplication(callee, arguments) =>
      val args = arguments.zipWithIndex.map({
        case (element, i) => generate(element).expression(Constants.ChildExpressions.argument(i))
      }).foldLeft[CodeGenWriter[List[cpp.Name]]](CodeGenWriter.pure(Nil))((a, b) => b ~: a).map(_.reverse)

      constructFunctionCall(generate(callee).expression(Constants.ChildExpressions.CALLEE), args)
    case e@Expression.CompoundExpression(_, Nil, result) => generate(result) // Optimize compound expressions without local definitions
    case e@Expression.CompoundExpression(_, localDefinitions, result) =>
      val scope_definitions = generate(localDefinitions)

      val CodeGenWriter(n, WriterTarget(helper_definitions)) = generate(result).expression(Constants.ChildExpressions.LAST)
      val result_using = UsingDefinition(Constants.LOCAL_SCOPE_RESULT_NAME, n)

      val scope = StructDefinition(Constants.LOCAL_SCOPE_DEFINITION, scope_definitions ::: helper_definitions ::: List(result_using)).templated(TemplateParameter(TypenameType, e.scope().scopeName) :: Nil)
      val fixed_scope = Constants.fix(cpp.Name(Constants.LOCAL_SCOPE_DEFINITION))

      CodeGenWriter(fixed_scope --> result_using.name, WriterTarget(scope :: Nil))
    case Expression.LambdaExpression(params, _, body) =>
      constructFunction(Constants.ANONYMOUS_FUNCTION, generate(body).expression(Constants.ChildExpressions.BODY), params.map(_.name), "lambda")
    case Expression.MatchExpression(matchee, cases) =>

      // TODO: If patterns don't overlap, they don't need to be separated into separate matches
      // TODO: Maybe there's a better way to force first match by using an incrementing index.

      def deductionHackNameForPath(qualifiedName: absyn.QualifiedName) = Constants.PATTERN_MATCH_DEDUCTION_HACK_PREFIX + qualifiedName.qualifier.mkString("_")

      def generate_type_expression_in_pattern(typeExpression: TypeExpression): AST.Name = typeExpression match {
        case TypeExpression.NamedTypeExpression(name) =>
          generate(exp.context().lookup(name, analyse_if_needed = true).asInstanceOf[Table.DefinitionEntry].definition.location())
        case _ => ???
      }

      case class PatternTranslationResult(pattern_expression: cpp.Name, used_classes: Set[absyn.Definition.ClassDefinition]) {
        def map(f: cpp.Name => cpp.Name): PatternTranslationResult = PatternTranslationResult(f(pattern_expression), used_classes)

        def flatMap(f: cpp.Name => PatternTranslationResult): PatternTranslationResult = {
          val x = f(pattern_expression)
          PatternTranslationResult(x.pattern_expression, this.used_classes ++ x.used_classes)
        }
      }

      def gen(pattern: Pattern): PatternTranslationResult = {
        var wildcardCounter = 0
        var expressionCounter = 0

        def helper(p: Pattern): PatternTranslationResult = p match {
          case Pattern.NamedPattern(name) => PatternTranslationResult(name, Set())
          case Pattern.TuplePattern(patterns) =>
            val sub = patterns.map(helper)

            PatternTranslationResult(Constants.TPL_RUNTIME_TUPLE.<>(sub.map(_.pattern_expression), force = true), sub.flatMap(_.used_classes).toSet)
          case Pattern.ValuePattern(name, argumentPatterns) =>
            val sub = argumentPatterns.map(helper)

            // Find the class definition this value pattern uses
            val used = pattern.context().lookup(name.asInstanceOf[NamedTypeExpression].name, analyse_if_needed = true).asInstanceOf[Table.DefinitionEntry]
              .definition.asInstanceOf[absyn.Definition.ClassDefinition]

            PatternTranslationResult(deductionHackNameForPath(used.qualified_name()).<>(sub.map(_.pattern_expression), force = true), sub.flatMap(_.used_classes).toSet + used)
          case Pattern.ObjectPattern(expression) =>
            expressionCounter += 1
            PatternTranslationResult(generate(expression).value, Set())
          case WildcardPattern() =>
            wildcardCounter += 1
            PatternTranslationResult(Constants.WILDCARD_PATTERN_NAME + wildcardCounter, Set())
        }

        helper(pattern)
      }

      def genCases(cs: List[MatchCase], i: Int): CodeGenWriter[(String, List[absyn.Definition.ClassDefinition])] = {
        val matcher_name = Constants.matcher(i)

        val (definitions, used: List[absyn.Definition.ClassDefinition]) = cs match {
          case MatchCase(WildcardPattern(), body) :: _ =>
            val body_exp = generate(body).expression(Constants.ChildExpressions.BODY)

            (cpp.StructDefinition(matcher_name,
              body_exp.log.definitions :+ cpp.UsingDefinition(Constants.MATCH_VALUE, body_exp.value)
            ).templated(cpp.TemplateParameter(cpp.TypenameType, Constants.WILDCARD_PATTERN_NAME + 0) :: Nil) :: Nil
              , Nil)
          case MatchCase(NamedPattern(name), body) :: _ =>
            val body_exp = generate(body).expression(Constants.ChildExpressions.BODY)

            (cpp.StructDefinition(matcher_name,
              body_exp.log.definitions :+ cpp.UsingDefinition(Constants.MATCH_VALUE, body_exp.value)
            ).templated(cpp.TemplateParameter(cpp.TypenameType, name) :: Nil) :: Nil,
              Nil)
          case MatchCase(pattern, body) :: tail =>
            val tailCases = genCases(tail, i + 1).map(t => {
              (t._1 <> (AST.Name(Constants.MATCHEE_NAME) :: t._2.map(c => generate(c.location()).--><>(Constants.CLASS_TYPE_NAME).copy(isTemplate = true)))) --> Constants.MATCH_VALUE
            })

            // We can't fully specialize nested templates.
            // If there are no free variables, we add a dummy parameter, that is filled with a default value
            val has_no_free_variables = pattern.freeVariables.isEmpty && pattern.wildcardCount == 0

            val translatedPattern = gen(pattern)

            val deduction_hack_parameters = translatedPattern.used_classes.map((c: absyn.Definition.ClassDefinition) => {
              val typ = c.type_info() match {
                case GenericType(parameters, underlying: TplClassType) => underlying
                case t: TplClassType => t
              }

              TemplateParameter(
                TemplateType(List.fill(typ.constructor.get.arity)(TypenameType)),
                deductionHackNameForPath(c.qualified_name()),
                None /*Some(generate(c.location()).--><>(Constants.CLASS_TYPE_NAME))*/) // TODO: Remove default parameter
            }).toList

            // Primary template containing all subsequent cases
            val primary = cpp.StructDefinition(matcher_name,
              cpp.UsingDefinition(Constants.MATCH_VALUE, tailCases.value) :: Nil
            ).templated(cpp.TemplateParameter(cpp.TypenameType, Constants.MATCHEE_NAME) ::
              Option.when(has_no_free_variables)(cpp.TemplateParameter(cpp.TypenameType, Constants.DUMMY_NAME, Some(Constants.TPL_RUNTIME_UNDEFINED))).toList :::
              deduction_hack_parameters
            )

            // Specialication that realizes the current case
            val body_exp = generate(body).expression(Constants.ChildExpressions.BODY)

            val specialization_parameters =
              pattern.freeVariables.map(f => cpp.TemplateParameter(cpp.TypenameType, f.name)) ::: // free variables
                (1 to pattern.wildcardCount).map(Constants.WILDCARD_PATTERN_NAME + _).map(cpp.TemplateParameter(cpp.TypenameType, _)).toList ::: // wildcards
                Option.when(has_no_free_variables)(cpp.TemplateParameter(cpp.TypenameType, Constants.DUMMY_NAME)).toList :::
                deduction_hack_parameters.map(_.copy(defaultArgument = None))


            val specialization = cpp.StructDefinition(matcher_name.<>(
              translatedPattern.pattern_expression ::
                Option.when(has_no_free_variables)(SimpleName(Constants.DUMMY_NAME)).toList :::
                deduction_hack_parameters.map(_.name).map(SimpleName)
            ).asInstanceOf[cpp.BasicName],
              body_exp.log.definitions :+ cpp.UsingDefinition(Constants.MATCH_VALUE, body_exp.value)
            ).templated(specialization_parameters, force_even_if_no_parameters = true)

            (tailCases.log.definitions ::: List(primary, specialization), translatedPattern.used_classes.toList)
          case Nil =>

            /* When there are no more cases, we generate one additional case: A case that always results in an compile time error.
               This gives us a little control over the error message displayed by the C++ compiler
               A naive implementation would be static_assert(false, "Match Error"), but C++ compilers evaluate this static assert even before template instantiation,
               meaning that the error is triggered even when the case is never reached.

               Some compilers (gcc) can be tricked by using something like static_assert(!is_same_v<void, void>, "Match Error") instead.
               To trick the compiler in a standard conform way, we need to make the failing static_assert depend on a template parameter.
               Luckily, with the matchee we always have a template parameter we can use for this.

               Sefl(2021) provides a solution for this:
               template <typename T>
               struct always_false {
                  static const bool value = false;
               };

               template <typename T>
               struct failing_case {
                 static_assert(always_false <T>::value );
               };

               The definition "always_false" is contained in the runtime library and is used here.
            */
            (cpp.StructDefinition(matcher_name,
              StaticAssertFailure((Constants.TPL_RUNTIME_NAMESPACE --> "always_false").<>(Constants.MATCHEE_NAME).-->("value"), "Match Error") :: cpp.UsingDefinition(Constants.MATCH_VALUE, Constants.TPL_RUNTIME_UNDEFINED) :: Nil
            ).templated(cpp.TemplateParameter(cpp.TypenameType, Constants.MATCHEE_NAME) :: Nil) :: Nil, Nil)
        }

        CodeGenWriter((matcher_name, used), WriterTarget(definitions))
      }


      val pat_results = cases.map(_.pattern).map(gen)

      val all_used = pat_results.map(_.used_classes).foldLeft(Set[absyn.Definition.ClassDefinition]())(_.union(_)).toList

      def genCases2(cs: List[MatchCase]) = {

        val any_with_no_free =
          cs.map(_.pattern).exists(pat => pat.freeVariables.isEmpty && pat.wildcardCount == 0) && all_used.isEmpty

        val deduction_hack_parameters = all_used.map((c: absyn.Definition.ClassDefinition) => {
          val typ = c.type_info() match {
            case GenericType(parameters, underlying: TplClassType) => underlying
            case t: TplClassType => t
          }

          TemplateParameter(TemplateType(List.fill(typ.constructor.get.arity)(TypenameType)), deductionHackNameForPath(c.qualified_name()), None /*Some(generate(c.location()).--><>(Constants.CLASS_TYPE_NAME))*/) // TODO: Remove default parameter
        })

        // Primary template containing all subsequent cases
        val primary = cpp.StructDefinition("_tpl_matcher",
          cpp.UsingDefinition(Constants.MATCH_VALUE,
            SimpleName("_tpl_matcher").<>(List(
              SimpleName("_tpl_matchindex + 1"), // Increment index
              SimpleName(Constants.MATCHEE_NAME), // Pass matchee to next case
            ) :::
              deduction_hack_parameters.map(_.name).map(SimpleName) //  Carry deduction hacks
            ).-->(Constants.MATCH_VALUE)) :: Nil
        ).templated(
          cpp.TemplateParameter(IntTemplateParameterType, "_tpl_matchindex") ::
            cpp.TemplateParameter(cpp.TypenameType, Constants.MATCHEE_NAME) ::
            deduction_hack_parameters :::
            Option.when(any_with_no_free)(cpp.TemplateParameter(cpp.TypenameType, Constants.DUMMY_NAME, Some(Constants.TPL_RUNTIME_UNDEFINED))).toList
        )

        val match_error =
          AST.StructDefinition(TemplatedName(
            "_tpl_matcher",
            List(
              SimpleName(cs.length.toString),
              SimpleName(Constants.MATCHEE_NAME)
            )
              ::: deduction_hack_parameters.map(_.name).map(SimpleName)
              ::: Option.when(any_with_no_free)(SimpleName(Constants.DUMMY_NAME)).toList
          ),
            List(
              StaticAssertFailure((Constants.TPL_RUNTIME_NAMESPACE --> "always_false").<>(Constants.MATCHEE_NAME).-->("value"), "Match Error"),
              CppAbsyn.AST.UsingDefinition(Constants.MATCH_VALUE, Constants.TPL_RUNTIME_UNDEFINED)
            )
          ).templated(List(
            cpp.TemplateParameter(cpp.TypenameType, Constants.MATCHEE_NAME),
          )
            ::: deduction_hack_parameters.map(_.copy(defaultArgument = None))
            ::: Option.when(any_with_no_free)(cpp.TemplateParameter(cpp.TypenameType, Constants.DUMMY_NAME)).toList
            , force_even_if_no_parameters = true)

        val cases: List[CppAbsyn.AST.Definition] = cs.zipWithIndex.map({
          case (MatchCase(pattern, body), i) =>
            val pat = gen(pattern)
            val body_exp = generate(body).expression(Constants.ChildExpressions.BODY)

            val specialization_parameters = (
              pattern.freeVariables.map(f => cpp.TemplateParameter(cpp.TypenameType, f.name)) // free variables
                ::: (1 to pattern.wildcardCount).map(Constants.WILDCARD_PATTERN_NAME + _).map(cpp.TemplateParameter(cpp.TypenameType, _)).toList // wildcards
                ::: deduction_hack_parameters.map(_.copy(defaultArgument = None))
                ::: Option.when(any_with_no_free)(cpp.TemplateParameter(cpp.TypenameType, Constants.DUMMY_NAME)).toList
              )

            AST.StructDefinition(TemplatedName(
              "_tpl_matcher",
              List(
                SimpleName(i.toString),
                pat.pattern_expression
              )
                ::: deduction_hack_parameters.map(_.name).map(SimpleName)
                ::: Option.when(any_with_no_free)(SimpleName(Constants.DUMMY_NAME)).toList
            ),
              body_exp.log.definitions :+ cpp.UsingDefinition(Constants.MATCH_VALUE, body_exp.value)
            ).templated(specialization_parameters, force_even_if_no_parameters = true)
        })

        (primary :: cases) :+ match_error
      }

      //Some(generate(c.location()).--><>(Constants.CLASS_TYPE_NAME))

      if (true) {
        val matchee_exp = generate(matchee).expression(Constants.ChildExpressions.MATCHEE);
        //matcher_name <- genCases(cases, 0)
        val matcher = genCases2(cases)

        CodeGenWriter(
          SimpleName("_tpl_matcher").<>(
            List(
              SimpleName("0"),
              matchee_exp.value
            )
              ::: all_used.map(c => generate(c.location()).--><>(Constants.CLASS_TYPE_NAME).copy(isTemplate = true))
          ).-->(Constants.MATCH_VALUE),
          WriterTarget(
            matchee_exp.log.definitions ++ matcher
          )
        )
      } else {

        for (matchee_exp <- generate(matchee).expression(Constants.ChildExpressions.MATCHEE);
             //matcher_name <- genCases(cases, 0)
             res <- genCases(cases, 0)
             ) yield

          res._1.<>(matchee_exp :: res._2.map(c => {
            generate(c.location()).--><>(Constants.CLASS_TYPE_NAME).copy(isTemplate = true)
          })) --> Constants.MATCH_VALUE
      }
  }

  case class WriterTarget(definitions: List[cpp.Definition]) {
    def concat(other: WriterTarget): WriterTarget = WriterTarget(this.definitions ::: other.definitions)

    def hasDefinitions: Boolean = definitions.nonEmpty
  }

  object WriterTarget {
    def empty: WriterTarget = WriterTarget(Nil)
  }

  case class CodeGenWriter[+T](value: T, log: WriterTarget) {
    def map[U](f: T => U): CodeGenWriter[U] = this.flatMap(x => CodeGenWriter.pure(f(x)))

    def flatMap[U](f: T => CodeGenWriter[U]): CodeGenWriter[U] = {
      val CodeGenWriter(b, newLog) = f(this.value)
      CodeGenWriter[U](b, this.log.concat(newLog))
    }

    def liftDefinitions[U](f: (T, List[cpp.Definition]) => U): CodeGenWriter[U] = this.map(e => f(e, this.log.definitions)).copy(log = WriterTarget.empty)
  }

  object CodeGenWriter {
    def pure[A](a: A): CodeGenWriter[A] = CodeGenWriter(a, WriterTarget.empty)
  }

  def pure[A](a: A): CodeGenWriter[A] = CodeGenWriter.pure(a)

  implicit class RichListOfCodeGenWrites[T](self: List[CodeGenWriter[T]]) {
    def trans: CodeGenWriter[List[T]] = self.foldRight[CodeGenWriter[List[T]]](pure(Nil))((a, b) => b.flatMap(t => a.map(_ :: t)))
  }

  implicit class RichMultipleCodeGenWriter[T](self: CodeGenWriter[List[T]]) {
    def ~:(other: CodeGenWriter[T]): CodeGenWriter[List[T]] = self.flatMap(tail => other.map(_ :: tail))
  }

  implicit class RichSingleCodeGenWriter(self: CodeGenWriter[cpp.Name]) {
    def compose(containerName: cpp.BasicName, containerCaptureNames: List[String], valueName: String, valueCaptureNames: List[String], dont_optimize: Boolean = false): CodeGenWriter[cpp.Name] = {
      if (!dont_optimize && !options.dont_optimize && valueCaptureNames.isEmpty) {
        if (!self.log.hasDefinitions) return self

        // TODO: These two optimizations can potentially be combined into one, general pattern
        self.log.definitions match {
          case Nil =>
            return self
          case List(d@ClassDefinition(name: SimpleName, _, _))
            if name == self.value
              && containerName.isInstanceOf[SimpleName]
              && containerCaptureNames.isEmpty // This may be more conservative than necessary
          =>
            return CodeGenWriter(containerName, WriterTarget(List(d.copy(name = containerName))))
          case _ =>
        }
      }

      val usageBase = containerName.baseName
      val usageWithContainerTemplate = usageBase.<>(containerCaptureNames.map(cpp.SimpleName))
      val usageWithAccess = usageWithContainerTemplate --> valueName.<>(valueCaptureNames.map(cpp.SimpleName))

      val valueDefinitionBase = cpp.UsingDefinition(cpp.SimpleName(valueName), self.value)
      val valueWithTemplate = valueDefinitionBase.templated(valueCaptureNames.map(n => cpp.TemplateParameter(cpp.TypenameType, n)))

      // TODO: Change for classes with private is located here
      //val definitionBase = cpp.StructDefinition(containerName, self.log.definitions :+ valueWithTemplate)
      val definitionBase = cpp.ClassDefinition(containerName, self.log.definitions, valueWithTemplate :: Nil)
      val definitionWithTemplate = definitionBase.templated(containerCaptureNames.map(n => cpp.TemplateParameter(cpp.TypenameType, n)))

      CodeGenWriter(usageWithAccess, WriterTarget(definitionWithTemplate :: Nil))
    }

    def lambdaImplementation(implementationName: String, parameters: List[String]): CodeGenWriter[cpp.Name] = compose(implementationName, parameters, Constants.FUNCTION_APPLY_RESULT, Nil)

    def lambda(lambdaName: String, parameters: List[String], tosName: String): CodeGenWriter[cpp.Name] = {
      val tos = FunctionDefinition(Constants.TOSTRING_NAME, StringLiteral(s"<$tosName>"))

      self.flatMap(n => CodeGenWriter(n, WriterTarget(tos :: Nil))).
        compose(lambdaName, Nil, Constants.APPLY_NAME, parameters).map(_ => lambdaName)
    }

    def expression(expressionName: String, dont_optimize: Boolean = false): CodeGenWriter[cpp.Name] = compose(expressionName, Nil, Constants.EXPRESSION_VALUE, Nil, dont_optimize)
  }

  implicit class RichDefinitionCodeGenWriter(self: CodeGenWriter[List[cpp.Definition]]) {
    def dropDefinitions: CodeGenWriter[Unit] = CodeGenWriter((), self.log.concat(WriterTarget(self.value)))
  }
}
