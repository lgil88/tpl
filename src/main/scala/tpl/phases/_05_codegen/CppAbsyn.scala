package tpl.phases._05_codegen

import scala.language.{existentials, implicitConversions}

object CppAbsyn {

  object AST {
    sealed trait TemplateParameterType
    case object TypenameType extends TemplateParameterType
    case class TemplateType(parameters: List[TemplateParameterType]) extends TemplateParameterType
    case object IntTemplateParameterType extends TemplateParameterType

    case class TemplateParameter(typ: TemplateParameterType, name: String, defaultArgument: Option[Name] = None)

    case class TemplateSignature(params: List[TemplateParameter])

    sealed trait Name {
      def <>(l: List[Name], force: Boolean = false): Name = this match {
        case a if l.isEmpty && !force => a
        case s: SimpleName => TemplatedName(s, l)
        case QualifiedName(qualifier, name, _) => QualifiedName(qualifier, name.<>(l, force).asInstanceOf[BasicName], isTemplate = true)
        case TemplatedName(name, templateParameters) => ???
      }

      def <>(l: Name*): Name = this.<>(l.toList)

      def -->(name: Name): QualifiedName = {
        name match {
          case name: BasicName => QualifiedName(this, name, isTemplate = false)
          case QualifiedName(qualifier, name, _) => QualifiedName(this --> qualifier, name, isTemplate = false)
        }
      }

      def --><>(name: Name): QualifiedName = {
        name match {
          case name: BasicName => QualifiedName(this, name, isTemplate = true)
          case QualifiedName(qualifier, name, _) => QualifiedName(this --> qualifier, name, isTemplate = true)
        }
      }

      def hasAnyTemplateInstanciation: Boolean = this match {
        case SimpleName(name) => false
        case TemplatedName(name, templateParameters) => true
        case QualifiedName(qualifier, name, _) => name.hasAnyTemplateInstanciation || qualifier.hasAnyTemplateInstanciation
      }
    }

    object Name {
      def apply(n: String): SimpleName = SimpleName(n)
    }

    sealed trait BasicName extends Name {
      def baseName: String = this match {
        case SimpleName(name) => name
        case TemplatedName(name, _) => name.baseName
      }
    }

    case class SimpleName(name: String) extends BasicName

    case class TemplatedName(name: SimpleName, templateParameters: List[Name]) extends BasicName

    case class QualifiedName(qualifier: Name, name: BasicName, isTemplate: Boolean) extends Name

    sealed trait Definition

    case class NamespaceDefinition(name: SimpleName, contents: List[Definition]) extends Definition

    case class TemplatedDefinition(signature: TemplateSignature, definition: TemplateableDefinition) extends Definition

    case class StaticAssertFailure(template_parameter_type_expression: Name, message: String) extends Definition

    case class FunctionDefinition(name: String, body: Expression) extends Definition

    sealed trait Expression

    case class StringConcatenation(components: List[Expression]) extends Expression

    case class StringLiteral(s: String) extends Expression

    case class FunctionCall(name: Name) extends Expression

    sealed trait PreprocessorDirective

    case object PragmaOnce extends PreprocessorDirective

    case class IncludeLocal(file: String) extends PreprocessorDirective

    case class IncludeGlobal(file: String) extends PreprocessorDirective

    sealed trait TemplateableDefinition extends Definition

    case class UsingDefinition(name: SimpleName, typ: Name) extends TemplateableDefinition

    case class StructDefinition(name: BasicName, contents: List[Definition]) extends TemplateableDefinition

    case class ClassDefinition(name: BasicName, private_contents: List[Definition], public_contents: List[Definition]) extends TemplateableDefinition

    case class StructDeclaration(name: SimpleName) extends TemplateableDefinition

    case class Program(directives: List[PreprocessorDirective], definitions: List[Definition])
  }

  object ConcreteSyntaxHelpers {

    import AST._

    /*Hooray for context sensitive grammar!*/
    case class Context(in_template: Boolean, template_parameter_names: Set[String], defined_names: Set[String], renamings: Map[String, String], dependent_scopes: Set[String]) {
      def resolveName(name: String): String = renamings.getOrElse(name, name)

      def defineNames(names: List[String]): Context = this.copy(defined_names = defined_names ++ names.toSet)

      def defineTemplateParameters(names: List[String]): Context = this.copy(template_parameter_names = this.template_parameter_names ++ names.toSet).defineNames(names)

      def rewriteDefinedNames(names: List[String], scope_name: Option[String]): Context = {
        // Names that can't be defined in the current context.
        val illegalNames = template_parameter_names ++ scope_name.toSet

        val rewritings = names.foldLeft[List[String]](Nil)((already_rewritten, n) => {

          def isAllowedRewriting(n: String) = {
            !illegalNames.contains(n) && !this.defined_names.contains(n) && !already_rewritten.contains(n) && !names.contains(n)
          }

          val a = {
            if (illegalNames.contains(n))
              LazyList.from(0).map(n + "_RENAMED_" + _).find(isAllowedRewriting).get
            else n
          }

          a :: already_rewritten
        }).reverse

        this.copy(renamings = renamings ++ (names zip rewritings).toMap)
      }

      def isDependent(name: Name): Boolean = {
        name match {
          case SimpleName(name) => this.dependent_names.contains(name)
          case TemplatedName(name, templateParameters) => this.isDependent(name) || templateParameters.exists(this.isDependent)
          case QualifiedName(qualifier, name, _) => this.isDependent(qualifier) || this.isDependent(name)
        }
      }

      def withScopes(scopes: List[String]): Context = {
        if (this.in_template) this.copy(dependent_scopes = dependent_scopes ++ scopes.toSet)
        else this
      }

      def enterTemplate: Context = this.copy(in_template = true)

      private lazy val dependent_names = this.template_parameter_names ++ dependent_scopes
    }

    object Context {
      def empty: Context = Context(in_template = false, Set(), Set(), Map(), Set())
    }

    implicit class RichProgram(self: Program) {
      def toCpp: String =
        self.directives.map(_.toCpp).mkString("\n") +
          self.definitions.map(d => "\n\n" + d.toCpp(Context.empty)).mkString
    }

    implicit class RichPreprocessorDirective(self: PreprocessorDirective) {
      def toCpp: String = self match {
        case AST.PragmaOnce => "#pragma once"
        case AST.IncludeLocal(f) => s"""#include "$f""""
        case AST.IncludeGlobal(f) => s"""#include <$f>"""
      }
    }

    implicit class RichName(self: Name) {

      def toCpp(context: Context): String = self match {
        case SimpleName(name) => context.resolveName(name)
        case TemplatedName(name, templateParameters) => s"""${name.toCpp(context)}<${templateParameters.map(_.asTypeName(context)).mkString(", ")}>"""
        case QualifiedName(qualifier, name: TemplatedName, _) if context.isDependent(qualifier) => s"""${qualifier.toCpp(context)}::template ${name.toCpp(context)}"""
        case QualifiedName(qualifier, name, true) if context.isDependent(qualifier) => s"""${qualifier.toCpp(context)}::template ${name.toCpp(context)}"""
        case QualifiedName(qualifier, name, _) => s"""${qualifier.toCpp(context)}::${name.toCpp(context)}"""
      }

      def asTypeName(context: Context): String = self match {
        case name: QualifiedName if context.isDependent(name.qualifier) && !name.isTemplate => s"typename ${name.toCpp(context)}"
        case name: QualifiedName => s"${name.toCpp(context)}"
        case name => s"${name.toCpp(context)}"
      }
    }

    implicit class RichTemplateParameterType(typ: TemplateParameterType) {
      def toCpp(context: Context): String = typ match {
        case TypenameType => "typename"
        case IntTemplateParameterType => "int"
        case TemplateType(parameters) => s"template<${parameters.map(_.toCpp(context)).mkString(",")}> class"
      }
    }

    implicit class RichTemplateParameter(self: TemplateParameter) {
      def toCpp(context: Context): String = self match {
        case TemplateParameter(t, n, Some(d)) => s"${t.toCpp(context)} ${context.resolveName(n)} = ${d.toCpp(context)}"
        case TemplateParameter(t, n, None) => s"${t.toCpp(context)} ${context.resolveName(n)}"
      }
    }

    implicit class RichTemplateSignature(signature: TemplateSignature) {
      def toCpp(context: Context): (String, Context) = {
        val rewritten = context.rewriteDefinedNames(signature.params.map(_.name), None).enterTemplate
        val withTemplateParameters = rewritten.defineTemplateParameters(signature.params.map(_.name).map(rewritten.resolveName))

        (s"template <${signature.params.map(_.toCpp(withTemplateParameters)).mkString(", ")}>", withTemplateParameters)
      }
    }

    implicit class RichExpression(self: Expression) {
      def toCpp: String = self match {
        case StringConcatenation(components) => components.map(_.toCpp).mkString(" + ")
        case StringLiteral(s) => s"""std::string("$s")"""
        case FunctionCall(name) => s"${name.toCpp(Context.empty)}()"
      }
    }

    implicit class RichDefinition(definition: Definition) {

      def definitionName: Option[String] = definition match {
        case NamespaceDefinition(name, _) => Some(name.name)
        case TemplatedDefinition(_, definition) => definition.definitionName
        case UsingDefinition(name, _) => Some(name.name)
        case StructDefinition(name, _) => Some(name.baseName)
        case StructDeclaration(name) => Some(name.name)
        case FunctionDefinition(name, _) => Some(name)
        case StaticAssertFailure(_, _) => None
        case ClassDefinition(name, _, _) => Some(name.baseName)
      }

      def toCpp(context: Context): String = {
        def childContext(name: BasicName, contents: List[Definition]): Context = {
          val rewritten = context.rewriteDefinedNames(contents.flatMap(_.definitionName), Some(name.baseName)).enterTemplate

          val rewrittenNames = contents.flatMap(_.definitionName).map(rewritten.resolveName)

          rewritten.defineNames(rewrittenNames).withScopes(rewrittenNames)
        }

        definition match {
          case NamespaceDefinition(name, contents) =>
            s"""namespace ${name.toCpp(context)} {
               |${contents.map(_.toCpp(context)).mkString("\n\n").indent(2).stripLineEnd}
               |}""".stripMargin
          case TemplatedDefinition(signature, definition) =>
            val (sig, newC) = signature.toCpp(context)

            s"""$sig
               |${definition.toCpp(newC.enterTemplate)}""".stripMargin
          case UsingDefinition(name, typ) =>
            s"""using ${name.toCpp(context)} = ${typ.asTypeName(context)};"""
          case StructDefinition(name, Nil) => s"""struct ${name.toCpp(context)} {};""".stripMargin
          case StructDefinition(name, contents) =>
            val child = childContext(name, contents)

            s"""struct ${name.toCpp(context)} {
               |${contents.map(_.toCpp(child)).mkString("\n\n").indent(2).stripLineEnd}
               |};""".stripMargin
          case StructDeclaration(name) =>
            s"""struct ${name.toCpp(context)};"""
          case StaticAssertFailure(typename, message) =>
            s"""static_assert(${typename.toCpp(context)}, "$message");"""
          case FunctionDefinition(name, body) =>
            s"""static std::string $name() {
               |  return ${body.toCpp};
               |}""".stripMargin
          case ClassDefinition(name, private_contents, public_contents) =>
            val child = childContext(name, public_contents ++ private_contents)

            val privates = if (private_contents.isEmpty) "" else s"${private_contents.map(_.toCpp(child)).mkString("\n\n").indent(2).stripLineEnd}"
            val publics = if (public_contents.isEmpty) "" else s"${public_contents.map(_.toCpp(child)).mkString("\n\n").indent(2).stripLineEnd}"

            if (private_contents.isEmpty) {
              s"""struct ${name.toCpp(context)} {
                 |$publics
                 |};""".stripMargin
            } else if (public_contents.isEmpty){
              s"""struct ${name.toCpp(context)} {
                 |$privates
                 |};""".stripMargin
            } else {
              s"""struct ${name.toCpp(context)} {
                 |$privates
                 |$publics
                 |};""".stripMargin
            }
        }
      }
    }
  }

  object ConstructionHelpers {

    import AST._

    implicit def string2Name(s: String): SimpleName = SimpleName(s)

    implicit class RichTemplateableDefinition(self: TemplateableDefinition) {
      def templated(params: List[TemplateParameter], force_even_if_no_parameters: Boolean = false): Definition = params match {
        case Nil if !force_even_if_no_parameters => self
        case params => TemplatedDefinition(TemplateSignature(params), self)
      }
    }
  }
}
