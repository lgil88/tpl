package tpl.phases

import tpl.Table.SymbolTable
import tpl.absyn

package object _04_varalloc {
  sealed trait Location
  case class SimpleNamedLocation(name: String) extends Location
  case class ScopeLocation(scope: SymbolTable, name: String) extends Location
  case class ModuleLocation(module: absyn.Module, name: String) extends Location
  case class VariableLocation(base: Location) extends Location
  case class SingletonObjectLocation(base: Location) extends Location
  case class ConstructorLocation(base: Location) extends Location
  case object NoLocation extends Location
}
