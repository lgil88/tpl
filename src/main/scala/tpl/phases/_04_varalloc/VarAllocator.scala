package tpl.phases._04_varalloc

import tpl.absyn
import tpl.absyn.Definition.{FunctionDeclaration, ImportDefinition}
import tpl.absyn.Expression.MatchCase
import tpl.absyn.{Definition, Expression, Program}

object VarAllocator {

  private def allocate(cs: MatchCase): Unit = {
    cs.pattern.freeVariables.foreach(p => p.location := SimpleNamedLocation(p.name))
    allocate(cs.body)
  }

  private def allocate(expression: Expression): Unit = expression match {
    case Expression.TupleExpression(elements) => elements.foreach(allocate)
    case Expression.MemberAccess(obj, name) => allocate(obj)
    case Expression.LambdaExpression(params, _, body) =>
      params.foreach(p => p.location := SimpleNamedLocation(p.name))
      allocate(body)
    case Expression.FunctionApplication(callee, arguments) =>
      allocate(callee)
      arguments.foreach(allocate)
    case exp@Expression.CompoundExpression(_, localDefinitions, result) =>
      localDefinitions.foreach(d => d.location := ScopeLocation(exp.scope(), d.name))
      localDefinitions.foreach(allocate)
      allocate(result)
    case Expression.MatchExpression(matchee, cases) =>
      allocate(matchee)
      cases.foreach(allocate)
    case Expression.TypedExpression(base, typ) => allocate(base)
    case _ =>
  }

  private def allocate(definition: Definition): Unit = definition match {
    case Definition.FunctionDefinition(_, _, parameters, _, body) =>
      parameters.foreach(p => p.location := SimpleNamedLocation(p.name))
      allocate(body)
    case Definition.TraitDefinition(_, _, _, members) =>
      members.foreach(_.location := NoLocation)
      members.foreach(allocate)
    case Definition.ClassDefinition(_, _, parameters, _, members) =>
      members.foreach(_.location := NoLocation)
      parameters.foreach(_.location := NoLocation)
      members.foreach(allocate)
    case Definition.ObjectDefinition(_, _, members) =>
      members.foreach(_.location := NoLocation)
      members.foreach(allocate)
    case Definition.ValDefinition(_, _, value) =>
      definition.location := VariableLocation(definition.location())
      allocate(value)
    case _: FunctionDeclaration => // Nothing
    case _: ImportDefinition => // Nothing
  }

  private def allocate(module: absyn.Module): Unit = {
    module.definitions.foreach(d => d.location := ModuleLocation(module, d.name))
    module.definitions.foreach(allocate)
    module.submodules().foreach(allocate)
  }

  def apply(program: Program): Unit = allocate(program.root_module())
}
