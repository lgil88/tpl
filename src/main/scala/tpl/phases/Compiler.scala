package tpl.phases

import tpl.CommandLineOptions
import tpl.phases._02_parser.Parser
import tpl.phases._03_semantic.SemanticAnalysis
import tpl.phases._04_varalloc.VarAllocator
import tpl.phases._05_codegen.CodeGen

object Compiler {
  
  def apply(options: CommandLineOptions): Unit = {      
    
    val program = Parser.parseProject(options.inPaths)
    
    if (options.phase.contains(CommandLineOptions.ParsePhase)) {
      println("Parsed successfully")
      return
    }

    if (options.phase.contains(CommandLineOptions.AbsynPhase)) {
      println(program.toString)
      return
    }

    SemanticAnalysis(program)
    VarAllocator(program)
    new CodeGen(options)(program)
  }
}
