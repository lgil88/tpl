package tpl

case class CommandLineOptions(phase: Option[CommandLineOptions.Phase],
                              inPaths: List[String],
                              outDirectory: String,
                              dont_optimize: Boolean,
                              namespace: String)

object CommandLineOptions {
  sealed trait Phase
  case object ParsePhase extends Phase
  case object AbsynPhase extends Phase

  import scala.util.chaining.scalaUtilChainingOps

  private def parse(args: List[String])(options: CommandLineOptions): CommandLineOptions = {
    val identRegex = "([a-zA-Z_][a-zA-Z0-9_]*)".r
    
    args match {
      case "--parse" :: tail => options.copy(phase = Some(ParsePhase)).pipe(CommandLineOptions.parse(tail))
      case "--absyn" :: tail => options.copy(phase = Some(AbsynPhase)).pipe(CommandLineOptions.parse(tail))
      case "--in" :: in_directory :: tail => options.copy(inPaths = in_directory :: options.inPaths).pipe(CommandLineOptions.parse(tail))
      case "--out" :: out_directory :: tail => options.copy(outDirectory = out_directory).pipe(CommandLineOptions.parse(tail))
      case "--namespace" :: identRegex(name) :: tail => 
        options.copy(namespace = name).pipe(CommandLineOptions.parse(tail))
      case "--disable-optimization" :: tail => options.copy(dont_optimize = true).pipe(CommandLineOptions.parse(tail))
      case Nil => options
    }
  }

  def parse(args: Array[String]): CommandLineOptions = CommandLineOptions.parse(args.toList)(CommandLineOptions(None, Nil, "", dont_optimize = false, namespace = "TPLProgram"))
  def parse(args: String): CommandLineOptions = CommandLineOptions.parse(args.split("\\s"))
}
