package tpl

import tpl.phases.Compiler

object Main {

  def main(args: Array[String]): Unit = {

    try {
      Compiler(CommandLineOptions.parse(args))
    } catch {
      case e: TPLError =>
        println(e)
        sys.exit(1)
    }
  }
}
