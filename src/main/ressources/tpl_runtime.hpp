#pragma once

#include <string>

namespace _tpl_runtime {

    // Used to defer the evaluation of static asserts to when a template is actually instantiated.
    // Idea from Vit Sefl: Translating Lambda Calculus into C++ Templates
    template <typename T>
    struct always_false {
        static const bool value = false;
    };

    template<typename ...T>
    struct joined_string;
    
    template<typename el1, typename el2, typename ...rest>
    struct joined_string<el1, el2, rest...> {
        static std::string res() {
            return el1::_tpl_str() + ", " + joined_string<el2, rest...>::res();
        };
    };
    
    template<typename el1>
    struct joined_string<el1> {
        static std::string res() {
            return el1::_tpl_str();
        };
    };
    
    template<>
    struct joined_string<> {
        static std::string res() {
            return "";
        };
    };

    template<typename ...T>
    struct tuple {
        static std::string _tpl_str() {        
            return "(" + joined_string<T...>::res() + ")";
        };
    };
    
    template<template<typename> typename t>
    struct fix {
        using fixed = t<fix<t>>;
    };
    
    struct undefined;
}

