package tpl

import org.scalatest.Suites

import java.io.File

class AllTests extends Suites(Framework.TestSuite.parse(new File("tests")).construct) {
}
