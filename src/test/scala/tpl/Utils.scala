package tpl

import java.io.File

object Utils {
  
  def compile(f: File): Unit = {
    val name = f.getName.replaceFirst("[.][^.]+$", "")
    val p = f.getParentFile.getName

    val options = s"--in $f --out ./testsout/$p/$name"

    println(s"Running '$options'")

    tpl.phases.Compiler(CommandLineOptions.parse(options))
  }

  def getListOfFiles(dir: String): List[File] = {
    val d = new File(dir)
    if (d.exists && d.isDirectory) d.listFiles.filter(f => f.isFile || f.isDirectory).toList
    else List[File]()
  }
}
