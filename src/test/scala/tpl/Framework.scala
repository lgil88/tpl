package tpl

import org.scalatest
import org.scalatest.funsuite.AnyFunSuiteLike
import org.scalatest.{BeforeAndAfterAllConfigMap, ConfigMap, Suite, Suites}
import tpl.OsUtils.{Command, FinishedCommandResult, TimeoutResult}

import java.io.{BufferedReader, File, InputStreamReader}
import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Path, Paths}
import scala.collection.mutable
import scala.concurrent.duration.DurationInt
import scala.io.Source
import scala.reflect.io.Directory
import scala.util.Try

object Framework {

  sealed trait TestResult

  case object CompileError extends TestResult

  case object CompileSuccess extends TestResult

  case object RuntimeError extends TestResult

  case object Success extends TestResult

  case class TestSettings(name: String,
                          file: File,
                          include_lib: Boolean,
                          expected_output: Option[String],
                          expected_outcome: TestResult
                         ) {
    def should_be_run: Boolean = Seq(RuntimeError, Success).contains(expected_outcome)
  }

  case class ProcessResult(status: Int, stdout: String, stderr: String)

  def runCmd(cmd: String*): Option[ProcessResult] = {

    println(cmd.mkString(" "))

    import scala.concurrent.ExecutionContext.Implicits.global
    import scala.concurrent._

    def readAll(s: java.io.InputStream): String = {
      val builder = new mutable.StringBuilder()
      val reader = new BufferedReader(new InputStreamReader(s))

      var line: String = reader.readLine()

      while (line != null) {
        builder.append(line)
        builder.append(System.getProperty("line.separator"))
        line = reader.readLine()
      }

      builder.mkString
    }

    val p = new ProcessBuilder(cmd: _*).start()

    val f = Future(blocking({
      val stdout = readAll(p.getInputStream)
      val stderr = readAll(p.getErrorStream)

      ProcessResult(p.waitFor(), stdout, stderr)
    }))

    Try(Await.result(f, duration.Duration(30, "sec"))).toOption.orElse({
      p.destroy()
      None
    })
  }

  trait Compiler {
    def name: String
    def commandFor(input: Path, output: String): Command
  }

  case class Gpp(version: Int, cpp_version: String) extends Compiler {
    override def name: String = s"g++ $version ($cpp_version)"
    def commandFor(input: Path, output: String): Command =
      Command(input.getParent,
        Seq(s"g++-$version", "-ftemplate-depth=100000", "-Werror", s"-std=$cpp_version", "-I./", input.getFileName.toString, "-o", output)
      ).bashed()
  }

  case class Clang(version: Int, cpp_version: String) extends Compiler {
    override def name: String = s"clang++ $version ($cpp_version)"
    def commandFor(input: Path, output: String): Command =
      Command(input.getParent,
        Seq(s"clang++-$version", "-ftemplate-depth=100000", "-Werror", s"-std=$cpp_version", "-I./", input.getFileName.toString, "-o", output)
      ).bashed()
  }

  case class MSVC() extends Compiler {
    override def name: String = "MSVC"

    override def commandFor(input: Path, output: String): Command = {
      Files.write(input.getParent.resolve("msvc_build.bat"),
        s"""call \"D:\\Visual Studio\\2022\\Enterprise\\Common7\\Tools\\VsDevCmd.bat\"\ncl ${input.getFileName} /EHsc /Fe\"$output\"""".getBytes(StandardCharsets.UTF_8))

      Command(input.getParent,
        Seq("cmd.exe", "/c", "call", "msvc_build.bat")
      )
    }
  }

  sealed trait TestSuite {
    var parent: Option[TestSuite] = None
    def name: String
    def path: List[String] = parent.map(_.path).getOrElse(Nil) :+ name

    def construct: Suite
  }

  case class TestLeaf(settings: TestSettings) extends TestSuite {
    override val name: String = settings.name
    def outpath: Path = Paths.get(s"testsout/${path.mkString("/")}".replace(" ", "_"))

    override def construct: AnyFunSuiteLike = new AnyFunSuiteLike with BeforeAndAfterAllConfigMap {
      var optimize = true

      def getCommand(): String = {
        var options: String = ""

        options += s"--in ${settings.file.getPath} "
        if (!optimize) options += "--disable-optimization "
        if (settings.include_lib) options += s"--in stdlib "

        options += s"--out $outpath "

        options
      }

      override def beforeAll(configMap: ConfigMap): Unit = {
        optimize = configMap.get("optimize").exists(_.asInstanceOf[String].toBoolean)
      }

      override def suiteName: String = name

      var compilation_failed = false

      test("Compilation") {
        val cmd = getCommand()

        println(cmd)

        val f = () => tpl.phases.Compiler(CommandLineOptions.parse(cmd))

        if (settings.expected_outcome == CompileError) {
          val caught = intercept[TPLError](f())

          val actual = caught.toString
          println(actual)

          if (settings.expected_output.isDefined) assert(actual.contains(settings.expected_output.get))
        }
        else Try(f()).recover(pf => {
          compilation_failed = true
          fail(pf.toString)
        }).get
      }

      if (settings.should_be_run) {
        val compilers: Seq[Compiler] = Seq(
          Gpp(11, "c++17"),
          Gpp(11, "c++20"),
          Clang(12, "c++17"),
          Clang(12, "c++20"),
          //MSVC()
          /*
          Clang(12, "c++17"),
          Clang(12, "c++20"),
          Clang(11, "c++17"),
          Clang(11, "c++20"),
           */
        )

        for (compiler <- compilers) {
          test(compiler.name) {
            if (compilation_failed) fail("Compilation failed")

            Files.write(Paths.get(s"$outpath/test_main.cpp"), Source.fromResource("test_main.cpp").mkString.getBytes(StandardCharsets.UTF_8))

            compiler.commandFor(Paths.get(s"$outpath/test_main.cpp"), "out.x").exec(10.seconds) match {
              case compileRes: FinishedCommandResult =>
                val cr = compileRes
                settings.expected_outcome match {
                  case RuntimeError =>
                    scalatest.Assertions.assert(compileRes.exitCode != 0)
                  case Success =>
                    if (compileRes.exitCode != 0) {
                      scalatest.Assertions.fail(s"C++ Compiler failed!\n${compileRes.stderr}")
                    }

                    if (settings.expected_output.isDefined) {
                      Command(outpath, Seq("./out.x")).bashed().exec(2.seconds) match {
                        case runRes: FinishedCommandResult =>
                          val rr = runRes
                          scalatest.Assertions.assert(runRes.exitCode == 0)
                          scalatest.Assertions.assert(runRes.stdout.trim == settings.expected_output.get)
                        case timeout: TimeoutResult => fail(s"The program timed out after ${timeout.duration}")
                      }
                    }
                }
              case timeout: TimeoutResult =>
                fail(s"${timeout.command} timed out after ${timeout.duration}")
            }
          }
        }
      }
    }
  }

  case class TestNode(override val name: String, children: Seq[TestSuite]) extends TestSuite {

    this.children.foreach(_.parent = Some(this))

    override def construct: Suite = {
      val d = new File("testsout")

      if (d.isDirectory) new Directory(d).deleteRecursively()

      new Suites(children.map(_.construct): _*) {
        override def suiteName: String = name
      }
    }
  }

  object TestSuite {
    private val main_test_file = "test.tpl"

    def extractSettings(f: File): TestSettings = {
      if (f.isFile) {

        val src = scala.io.Source.fromFile(f)

        val settings = src.getLines().map(_.trim).takeWhile(_.startsWith("//")).map(_.drop(2).trim).foldLeft(TestSettings(f.getName, f, include_lib = false, None, Success)) {
          case (a, b) =>
            val key = b.takeWhile(_ != ':').trim
            val value = b.dropWhile(_ != ':').drop(1).trim

            key match {
              case "stdlib" => a.copy(include_lib = value.toBoolean)
              case "outcome" => a.copy(expected_outcome = value.trim match {
                case "compilererror" => CompileError
                case "compilersuccess" => CompileSuccess
                case "runtimeerror" => RuntimeError
                case "success" => Success
              })
              case "output" => a.copy(expected_output = Some(value.trim))
              case _ => a
            }
        }

        src.close()

        settings
      }
      else {
        val settings = extractSettings(f.listFiles().find(_.getName == main_test_file).get)

        settings.copy(name = if (settings.name == main_test_file) f.getName else settings.name, file = f)
      }
    }

    def parse(file: File): TestNode = {

      def p(file: File): TestSuite = {
        if (file.isDirectory) {
          val children = file.listFiles()

          if (children.exists(_.getName == main_test_file)) TestLeaf(extractSettings(file))
          else TestNode(file.getName, children.map(p))

        }
        else TestLeaf(extractSettings(file))
      }

      p(file) match {
        case l: TestLeaf => TestNode(l.settings.name, Seq(l))
        case n: TestNode => n
      }
    }
  }
}
