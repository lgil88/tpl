package tpl

import cats.effect.IO
import cats.effect.unsafe.IORuntime

import java.nio.charset.StandardCharsets.UTF_8
import java.nio.file.Path
import scala.concurrent.duration.{Duration, FiniteDuration}

object OsUtils {
  // Shamelessly stolen from https://alexn.org/blog/2022/10/03/execute-shell-commands-in-java-scala-kotlin

  sealed trait CommandResult {
    def command: Command
  }

  final case class FinishedCommandResult(
                                          override val command: Command,
                                          exitCode: Int,
                                          stdout: String,
                                          stderr: String,
                                        ) extends CommandResult

  final case class TimeoutResult(override val command: Command,
                                 duration: Duration) extends CommandResult


  final case class Command(cwd: Path, command: Seq[String]) {
    def string(): String = command.mkString(" ")
    def bashed(): Command = Command(cwd, "wsl" +: this.command)
    override def toString: String = command.map(c => s""""$c"""").mkString(" ")

    def exec(timeout: FiniteDuration): CommandResult = {
      IO.interruptible {
        println(this.string())
        val proc = Runtime.getRuntime.exec(this.command.toArray, null, this.cwd.toFile)
        try {
          val outBuilder = new StringBuilder
          val errBuilder = new StringBuilder

          while (proc.isAlive) {
            outBuilder.append(new String(proc.getInputStream.readNBytes(proc.getInputStream.available()), UTF_8))
            errBuilder.append(new String(proc.getErrorStream.readNBytes(proc.getErrorStream.available()), UTF_8))
          }

          outBuilder.append(new String(proc.getInputStream.readNBytes(proc.getInputStream.available()), UTF_8))
          errBuilder.append(new String(proc.getErrorStream.readNBytes(proc.getErrorStream.available()), UTF_8))

          FinishedCommandResult(
            command = this,
            exitCode = proc.exitValue(),
            stdout = outBuilder.result(),
            stderr = errBuilder.result()
          )
        } finally {
          proc.destroy()
        }
      }.timeoutTo(timeout, IO.pure(TimeoutResult(this, timeout))).unsafeRunSync()(IORuntime.global)
    }
  }
}
