#include "_tpl_module_.hpp"

#include <iostream>

template<typename T>
void print_tpl_result(){
    std::cout << T::_tpl_str() << "\n";
}

int main() {
    print_tpl_result<TPLProgram::root::test::main>();

    return 0;
}