let isEven :: Integer -> Bool
    isEven 0 = True
    isEven n = isOdd(n - 1)
    isOdd :: Integer -> Bool
    isOdd 0 = False
    isOdd n = isEven (n - 1)
 in isEven 5

fme: Integer -> Bool
fme (S _) = True
fme (S (S _)) = False


data Nat = Z | S Nat

x = S (S Z)

res =
case x of
   (S _) -> True
   (S (S _)) -> False
   _ -> False

res2 =
case x of
    S _ -> True
    x -> (case x of
      S (S _) -> False
      _ -> False
   )


main = do
    print res2


((\x -> x) (\x -> x))